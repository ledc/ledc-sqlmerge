package be.ugent.ledc.sqlmerge.propagation;

import java.io.PrintStream;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Console
{
    public static PrintStream printStream = System.out;
    
    private static final String ANSI_RESET = "\033[0m";
//    public static final String ANSI_RED = "\033[31m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\033[32m";
    public static final String ANSI_BRIGHT_BLUE = "\033[34;1m";
    public static final String ANSI_CYAN = "\033[36m";
    public static final String ANSI_MAGENTA = "\033[35m";
    
    public static boolean verbose = true;
    
    private int depth = 0;
    
    public void println(String message, String color)
    {
        printStream.println(prefix().concat(color + message + ANSI_RESET));
    }
    
    public void println(String message)
    {
        printStream.println(prefix().concat(message));
    }
    
    public void printlnVerbose(String message, String color)
    {
        if(verbose)
            println(color + message + ANSI_RESET);
    }
    
    public void printlnVerbose(String message)
    {
        if(verbose)
            println(message);
    }
    
    private String prefix()
    {
        return depth == 0
            ? ""
            : IntStream
                .rangeClosed(1, depth)
                .mapToObj(i->"   ")
                .collect(Collectors.joining("", "", "| "));
    }
    
    public void increaseDepth()
    {
        depth++;
    }
    
    public void decreaseDepth()
    {
        if(depth>0)
            depth--;
    }
    
    public String readLine()
    {
        return System.console() == null
            ? new Scanner(System.in).nextLine()
            : System.console().readLine();
    }
    
    public String readOptions(String... options)
    {
        return readOptions(false, options);
    }
    
    public String readOptions(boolean caseSensitive, String... options)
    {
        while(true)
        {
            String choice = readLine();

            if(choice != null && Stream.of(options)
                .filter(o -> o != null)
                .map(o -> caseSensitive ? o : o.toLowerCase())
                .anyMatch(o ->
                    o != null
                    && o.equals(
                        caseSensitive
                            ? choice
                            : choice.toLowerCase())
                )
            )
                return caseSensitive ? choice : choice.toLowerCase();

            println("Invalid option " + choice);            
        }
    }
    
    public String readPassword()
    {
        return System.console() == null
            ? new Scanner(System.in).nextLine()
            : new String(System.console().readPassword());
    }
    
}
