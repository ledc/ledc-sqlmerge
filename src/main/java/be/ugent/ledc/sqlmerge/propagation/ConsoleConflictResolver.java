package be.ugent.ledc.sqlmerge.propagation;

import be.ugent.ledc.core.binding.jdbc.agents.PersistentDatatype;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.datastructures.Multiset;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class ConsoleConflictResolver implements ConflictResolver
{
    @Override
    public <T> T resolve(TableSchema tableSchema, String attribute, Multiset<T> choices)
    {
        ConsoleFactory.getConsole().println("Conflict for " + attribute, Console.ANSI_RED);
        
        List<T> valueList = new ArrayList<>(choices.keySet());
        
        ConsoleFactory.getConsole().println("Values that are present: ", Console.ANSI_RED);
        
        int idx = 1;
        for(T value: valueList)
        {
            ConsoleFactory
            .getConsole()
            .println("-> " + (idx++) + ". " + value + ": " + choices.get(value)
                    + (choices.multiplicity(value) == 1 ? " time": " times"),
                Console.ANSI_RED);
        }
        
        boolean validInput = false;
        
        String input = "";
        
        while(!validInput)
        {
            ConsoleFactory.getConsole().println("Enter a resolution. Use /n to pass the index of one of the choices. Starting index is 1");
            input = ConsoleFactory.getConsole().readLine();
            
            if(!input.matches("/(\\d+)"))
                validInput = true;
            else if(Integer.parseInt(input.substring(1)) >= 1 && Integer.parseInt(input.substring(1)) <= valueList.size())
                validInput = true;
            else
                ConsoleFactory
                    .getConsole()
                    .println("Choice " + input + " has an index out of permitted range from 1 to " + valueList.size(), Console.ANSI_RED);
        }
        
        return input.matches("/(\\d+)")
            ? valueList.get(Integer.parseInt(input.substring(1)) - 1)
            : (T)parse(tableSchema, attribute, input);
    }
    
    @Override
    public DataObject fixFirstOrderViolation(TableSchema tableSchema, Dataset objectsToMerge, DataObject merged, DataObject parentMerge, Violation v)
    {
        Set<String> attributesToEdit = v
            .getViolatingAttributes()
            .stream()
            .filter(a -> !parentMerge.getAttributes().contains(a))
            .collect(Collectors.toSet());
        
        if(attributesToEdit.isEmpty())
            throw new RuntimeException("Second order violation with no attributes to edit.");
        
        DataObject fixed = new DataObject(merged);
        
        for(String a: attributesToEdit)
        {
            ConsoleFactory.getConsole()
            .println("Fixing attribute '" + a + "'");
            
            ConsoleFactory.getConsole()
            .println("Current value: " + merged.get(a));
        
            Multiset bag = new Multiset(objectsToMerge
                .getDataObjects()
                .stream()
                .filter(o -> o.get(a) != null)
                .map(o -> o.get(a))
                .collect(Collectors.toList()));
            
            List valueList = new ArrayList(bag.keySet());
        
            ConsoleFactory.getConsole().println("Values that are present: ");
        
            int idx = 1;
            for(Object value: valueList)
            {
                ConsoleFactory
                    .getConsole()
                    .println("-> "
                        + (idx++) + ". " + value + ": " + bag.get(value)
                        + (bag.multiplicity(value) == 1 ? " time": " times"));
            }
        
            boolean validInput = false;
        
            String input = "";

            while(!validInput)
            {
                ConsoleFactory
                    .getConsole()
                    .println("Enter a new value. Use /n to pass the index of one of the choices. Starting index is 1.");
                
                ConsoleFactory
                    .getConsole()
                    .println("To keep the merged value for this attribute, enter /0.");
                
                input = ConsoleFactory.getConsole().readLine();

                if(!input.matches("/(\\d+)"))
                    validInput = true;
                else if(Integer.parseInt(input.substring(1)) == 0)
                    validInput = true;
                else if(Integer.parseInt(input.substring(1)) >= 1 && Integer.parseInt(input.substring(1)) <= valueList.size())
                    validInput = true;
                else
                    ConsoleFactory
                        .getConsole()
                        .println("Choice " + input + " is not equal to zero and has an index out of permitted range from 1 to " + valueList.size(), Console.ANSI_RED);
            }
        
            fixed.set(a,
                input.matches("/(\\d+)")
                ? Integer.parseInt(input.substring(1)) == 0
                    ? merged.get(a)
                    : valueList.get(Integer.parseInt(input.substring(1)) - 1)
                : parse(tableSchema, a, input));
        }
        
        //Return the fixed object
        return fixed;
    }
    
    @Override
    public Dataset fixSecondOrderViolation(TableSchema tableSchema, Dataset merged, DataObject parentMerge, Violation v)
    {
        Set<String> attributesToEdit = v
            .getViolatingAttributes()
            .stream()
            .filter(a -> !parentMerge.getAttributes().contains(a))
            .collect(Collectors.toSet());
        
        if(attributesToEdit.isEmpty())
            throw new RuntimeException("Second order violation with no attributes to edit.");
        
        Dataset fixed = new SimpleDataset(merged.getDataObjects());
        
        for(DataObject violatingObject: v.getViolatingObjects().getDataObjects())
        {
            ConsoleFactory.getConsole()
            .println("Fixing object " + violatingObject, Console.ANSI_CYAN);
            
            ConsoleFactory.getConsole().increaseDepth();
            
            DataObject fix = new DataObject(violatingObject);
            
            for(String a: attributesToEdit)
            {
                ConsoleFactory.getConsole()
                .println("Fixing attribute '" + a + "'");

                ConsoleFactory
                    .getConsole()
                    .println("Enter a new value. To keep the merged value for this attribute, enter /0.");

                String input = ConsoleFactory.getConsole().readLine();

                fix.set(a,
                    input.matches("/0")
                    ? violatingObject.get(a)
                    : parse(tableSchema, a, input));
            }
            
            ConsoleFactory
                .getConsole()
                .println("Fixed to " + fix, Console.ANSI_GREEN);
            
            int index = fixed.getDataObjects().indexOf(violatingObject);
            fixed.getDataObjects().remove(index);
            fixed.getDataObjects().add(index, fix);
            
            ConsoleFactory.getConsole().decreaseDepth();
        }
        
        return fixed;
    }
    
    private Object parse(TableSchema tableSchema, String attribute, String stringValue)
    {
        return PersistentDatatype.parse(
            stringValue,
            (String) tableSchema.getAttributeProperty(
                attribute,
                TableSchema.PROP_DATATYPE)
        );
    }
    
    
}
