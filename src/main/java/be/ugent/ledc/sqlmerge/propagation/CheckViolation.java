package be.ugent.ledc.sqlmerge.propagation;

import be.ugent.ledc.core.dataset.DataObject;
import java.util.Set;

public class CheckViolation extends Violation
{
    private final String checkClause;

    public CheckViolation(String checkClause, DataObject violatingObject, Set<String> violatingAttributes)
    {
        super(violatingObject, violatingAttributes, ViolationType.CHECK);
        this.checkClause = checkClause;
    }

    public CheckViolation()
    {
        super(ViolationType.CHECK);
        this.checkClause = "";
    }

    @Override
    public String toString()
    {
        return isVoid()
            ? "No violation"
            : "Violation of "
                + getViolationType().name()
                + " constraint '" + checkClause + "' on attributes '"
                + getViolatingAttributes() + "'";
    }
    
    
    
}
