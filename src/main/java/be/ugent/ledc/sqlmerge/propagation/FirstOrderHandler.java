package be.ugent.ledc.sqlmerge.propagation;

import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.Multiset;
import be.ugent.ledc.sqlmerge.Merger;
import be.ugent.ledc.sqlmerge.MergingException;
import java.util.Set;
import java.util.stream.Collectors;

public class FirstOrderHandler
{    
    private final Merger<DataObject> merger;
    
    private final ConflictResolver conflictResolver = new ConsoleConflictResolver();

    public FirstOrderHandler(Merger<DataObject> merger)
    {
        this.merger = merger;
    }
    
    public DataObject merge(TableSchema tableSchema, Dataset objectsToMerge, DataObject parentMerge, MergeValidator handler) throws MergingException, DataReadException
    {
        ConsoleFactory.getConsole().println("Merging in table " + tableSchema.getName());
            
        if(!parentMerge.getAttributes().isEmpty())
            ConsoleFactory
            .getConsole()
            .println(" -> Parent merge: " + parentMerge);
            
        ConsoleFactory
        .getConsole()
        .println("Objects to merge:");
        
        for(DataObject o: objectsToMerge.getDataObjects())
        {
            ConsoleFactory
            .getConsole()
            .println(
                "-> " + o.inverseProject(parentMerge.getAttributes()).toString(),
                Console.ANSI_CYAN);
        }
        
        Multiset<DataObject> objectsAsBag = convertToMultiset(objectsToMerge);
        
        //Make an object that represents the merged
        final DataObject merged = merger
            .merge(objectsAsBag)
            .concat(parentMerge);
        
        //Check if we made a decision for all relevant attributes
        Set<String> expectedAttributes = objectsAsBag
            .keySet()
            .stream()
            .flatMap(o -> o.getAttributes().stream().filter(a -> o.get(a) != null))
            .collect(Collectors.toSet());
            
        if(expectedAttributes.stream().allMatch(a -> merged.get(a) != null))
        {
            //Succesfull fusion without conflicts
            ConsoleFactory.getConsole().printlnVerbose("Succesfull merge!");
        }
        else
        {
            ConsoleFactory.getConsole().println("Conflicts found! Start resolution...");

            for(String a: expectedAttributes)
            {
                Multiset valueBag = new Multiset<>(objectsAsBag
                    .keySet()
                    .stream()
                    .filter(o -> o.get(a) != null)
                    .map(o -> o.get(a))
                    .collect(Collectors.toList()));
                

                //Case 1: fusion was already taken care off
                if(merged.get(a) != null)
                {
                    ConsoleFactory
                    .getConsole()
                    .printlnVerbose(
                        "Merging of " + a + ": auto-resolve",
                        Console.ANSI_GREEN);
                }
                //Case 2: there is a single alternative: we take it
                else if(valueBag.size() == 1)
                {
                    merged.set(a, valueBag.keySet().stream().findFirst().get());

                    ConsoleFactory
                        .getConsole()
                        .printlnVerbose(
                            "Merging of " + a + ": auto-resolve",
                            Console.ANSI_GREEN
                        );
                }
                //Case 3: cannot resolve input
                else
                {
                    Object resolution = conflictResolver.resolve(
                        tableSchema,
                        a,
                        valueBag);

                    merged.set(a,resolution);

                    ConsoleFactory
                        .getConsole()
                        .printlnVerbose(
                            "Merging of " + a + ": resolved to '" + resolution + "'",
                            Console.ANSI_GREEN);
                }
            }
        }
        
        return merged;

    }

    public Merger<DataObject> getMerger()
    {
        return merger;
    }
    
    private Multiset<DataObject> convertToMultiset(Dataset dataset)
    {
        //Compose a bag
        Multiset<DataObject> objectsToMergeBag = new Multiset<>();
        
        dataset.getDataObjects().stream().forEach(objectsToMergeBag::add);
        
        return objectsToMergeBag;
    }
}
