package be.ugent.ledc.sqlmerge.propagation;

public enum ViolationType
{
    CHECK,
    UNIQUE,
    NOT_NULL,
    PRIMARY_KEY_PRESERVATION,
    FOREIGN_KEY_PRESERVATION,
    ANY
}
