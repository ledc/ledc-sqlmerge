package be.ugent.ledc.sqlmerge.propagation;

import be.ugent.ledc.core.binding.BindingException;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.binding.jdbc.JDBCBinder;
import be.ugent.ledc.core.binding.jdbc.JDBCDataReader;
import be.ugent.ledc.core.binding.jdbc.schema.DatabaseSchema;
import be.ugent.ledc.core.binding.jdbc.schema.Reference;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.sqlmerge.MergingException;
import be.ugent.ledc.sqlmerge.secondorder.CascadeMerger;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class PropagationEngine
{
    /**
     * The merge strategy that holds, per table, ways to perform merges in that table.
     */
    private final MergeStrategy mergeStrategy;
    
    /**
     * A schema of the database in which the merge is performed
     */
    private final DatabaseSchema databaseSchema;
    
    /**
     * A code generator object to build SQL statements that reflect that choices made during merge
     */
    private final PropagationSQLGenerator codeGenerator;
    
    private final JDBCDataReader dataReader;
    
    private final MergeValidator mergeValidator;
    
    private final ConflictResolver conflictResolver;
    
    private final boolean manualMerging;
    
    private final String schema;

    public PropagationEngine(MergeStrategy mergeStrategy, JDBCBinder binder, boolean manualMerging) throws BindingException
    {
        this.mergeStrategy = mergeStrategy;
        this.codeGenerator = new PropagationSQLGenerator(binder.getRdb().getVendor().getAgent().escaping());
        this.databaseSchema = binder.getSchema();
        this.dataReader = new JDBCDataReader(binder);
        this.mergeValidator = new MergeValidator(binder);
        this.conflictResolver = new ConsoleConflictResolver();
        this.manualMerging = manualMerging;
        this.schema = binder.getRdb().getSchemaName();
    }
    
    public PropagationEngine(MergeStrategy mergeStrategy, JDBCBinder binder) throws BindingException
    {
        this(mergeStrategy, binder, false);

    }

    public List<String> startMerge(TableSchema tableSchema, Dataset objectsToFuse) throws MergingException, DataReadException, SQLException
    {
        List<String> sqlScript = new ArrayList<>();

        mergeRowsInTable(tableSchema, objectsToFuse, new DataObject(), sqlScript);

        return sqlScript;
    }
    
    public void close() throws SQLException
    {
        if(dataReader != null)
            dataReader.closeConnection();
    }
    
    private DataObject mergeRowsInTable(TableSchema tableSchema, Dataset objectsToMerge, DataObject parentMerge, List<String> sqlScript) throws MergingException, SQLException, DataReadException
    {           
        DataObject merged;
                
        //Check conditions for cascade delete
        if(isCascadeDelete(tableSchema, parentMerge))
        {
            print("-> Cascading strategy detected: auto-resolve", Console.ANSI_BRIGHT_BLUE);
            
            //Merge is the object matching the parentMerge
            merged = objectsToMerge
                .select(o -> parentMerge.isProjectionOf(o))
                .getDataObjects()
                .get(0);
        }
        else
        {
            //Compute the merge
            merged = mergeStrategy
                .getFirstOrderHandler(tableSchema.getName())
                .merge(
                    tableSchema,
                    objectsToMerge,
                    parentMerge,
                    mergeValidator);

            printVerbose("Validating merge");

            Violation next = mergeValidator.validateFirstOrderMerge(
                tableSchema,
                objectsToMerge,
                parentMerge,
                merged);

            while(!next.isVoid())
            {
                //Report
                print(next.toString(), Console.ANSI_RED);
                
                merged = conflictResolver.fixFirstOrderViolation(
                    tableSchema,
                    objectsToMerge,
                    merged,
                    parentMerge,
                    next);

                //Re validate
                next = mergeValidator
                .validateFirstOrderMerge(
                    tableSchema,
                    objectsToMerge,
                    parentMerge,
                    merged);
            }
        }
        
        print("Merged objects to " + merged, Console.ANSI_GREEN);

        //Inspect the database schema for referencing relations
        for(String linkedTableName: databaseSchema.keySet())
        {
            //Fetch table schema
            TableSchema linkedTableSchema = databaseSchema.get(linkedTableName);

            //Check
            if(linkedTableSchema.referencesTo(tableSchema))
                startPropagation(
                    tableSchema,
                    linkedTableSchema,
                    objectsToMerge,
                    merged,
                    sqlScript);
        }
        
        //Add the cleaning part: n-1 delete statements
        codeGenerator.firstOrderSqlClean(
            objectsToMerge,
            merged,
            tableSchema,
            sqlScript,
            mergeValidator.getChangeLog(),
            schema);
        
        //Add the merge part: update statement
        codeGenerator.firstOrderSqlMerge(
            objectsToMerge,
            merged,
            tableSchema,
            sqlScript,
            mergeValidator.getChangeLog(),
            schema);
        
        return merged;
    }
    
    private void startPropagation(TableSchema tableSchema, TableSchema linkedTableSchema, Dataset objectsMerged, DataObject merged, List<String> sqlScript) throws SQLException, DataReadException, MergingException
    {
        shiftRight();
        print("Propagating to " + linkedTableSchema.getName() + "", Console.ANSI_BRIGHT_BLUE);
           
        //Fetch the references between signatures
        List<Reference> references = linkedTableSchema
            .getReferences()
            .stream()
            .filter(reference -> reference
                .getReferenced()
                .getName()
                .equals(tableSchema.getName()))
            .collect(Collectors.toList());
         
        for(Reference reference: references)
        {
            //Make projection key
            Set<String> projectionKey = constructProjectionKey(reference, linkedTableSchema);
            printVerbose("Reference:" + reference);
            printVerbose("FK project = " + mergeStrategy
                .getSecondOrderHandler(linkedTableSchema.getName())
                .isForeignKeyProject());
            printVerbose("Linked objects identified by " + projectionKey);
            
            Dataset linkedConditions = objectsMerged.project(reference.values());
            
            DataObject parentMerge = null;
            
            if(merged != null)
            {
                parentMerge = merged.project(reference.values());
            
                for(String linkedTableAttribute: reference.keySet())
                {
                    linkedConditions
                        .getDataObjects()
                        .stream()
                        .forEach(o -> o.rename(
                            reference.get(linkedTableAttribute),
                            linkedTableAttribute));

                    parentMerge.rename(
                        reference.get(linkedTableAttribute),
                        linkedTableAttribute);
                }
            }

            //Check the type of propagation
            if(projectionKey.equals(reference.keySet()))
            {
                print("Reference from " + linkedTableSchema.getName() + " to "
                    + tableSchema.getName()
                    + " is 1-to-1: propagated merge is first order.", Console.ANSI_BRIGHT_BLUE);

                //Collect the linked objects
                Dataset objectsToMerge = dataReader
                    .readContractedData(
                        codeGenerator.generateSelectFromWhereCode(
                            null,
                            linkedTableSchema.getName(),
                            linkedConditions.getDataObjects(),
                            schema
                        ));
                
                //No linked objects: empty merge
                if(objectsToMerge.getSize() == 0
                || objectsToMerge.getDataObjects().stream().allMatch(o->o.getAttributes().isEmpty())
                )
                {
                    print("No linked objects: merge is empty", Console.ANSI_GREEN);
                }
                //If the merge that cause propagation yield null, we should cascade delete rows
                else if(merged == null)
                {
                    cascadeDelete(
                        linkedTableSchema,
                        objectsToMerge,
                        null,
                        sqlScript);
                }
                //If there is a single original: do a cascaded update if the referenced attributes
                //are affected
                else if(objectsMerged.getSize() == 1)
                {
                    DataObject original = objectsMerged
                        .getDataObjects()
                        .get(0);

                    if(original.diff(merged).stream().anyMatch(a -> reference.values().contains(a)))
                    {
                        Dataset linkedData = dataReader
                        .readContractedData(
                            codeGenerator.generateSelectFromWhereCode(
                                null,
                                linkedTableSchema.getName(),
                                linkedConditions.getDataObjects(),
                                schema
                            ));

                        for(DataObject o: linkedData.getDataObjects())
                        {
                            cascadeUpdate(
                                linkedTableSchema,
                                o,
                                o.concat(parentMerge),
                                sqlScript);
                        }
                    }
                }
                //Else we should propagate via a recursive merge
                else
                {
                    //Start the recursive first order merge 
                    mergeRowsInTable(
                        linkedTableSchema,
                        objectsToMerge, 
                        parentMerge, 
                        sqlScript);
                }
            }
            else
            {
                print("Reference from " + linkedTableSchema.getName() + " to "
                    + tableSchema.getName()
                    + " is 1-to-N: propagated merge is second order.", Console.ANSI_BRIGHT_BLUE);
                             
                //Initialize linked sources
                List<Dataset> linkedDatasets = new ArrayList<>();

                Set<String> queries = linkedConditions
                    .getDataObjects()
                    .stream()
                    .map(o -> codeGenerator
                        .generateSelectFromWhereCode(
                            linkedTableSchema.getAttributeNames(),
                            linkedTableSchema.getName(),
                            o,
                            schema))
                    .collect(Collectors.toSet());

                for(String query: queries)
                {
                    linkedDatasets.add(dataReader.readContractedData(query));
                }

                if(linkedDatasets.stream().allMatch(d -> d.getSize() == 0)
                || linkedDatasets.stream().allMatch(d-> d.getDataObjects().stream().allMatch(o->o.getAttributes().isEmpty()))
                )
                {
                    print("No linked objects: merge is empty", Console.ANSI_GREEN);
                }
                //If the merged object is null, it means delete cascade.
                else if(merged == null)
                {
                    cascadeDelete(
                        linkedTableSchema,
                        new SimpleDataset(
                            linkedDatasets
                            .stream()
                            .flatMap(d -> d.getDataObjects().stream())
                            .collect(Collectors.toList())),
                        null,
                        sqlScript);
                }
                //If there is a single original: do a cascaded update if the referenced attributes
                //are affected
                else if(objectsMerged.getSize() == 1)
                {
                    DataObject original = objectsMerged
                        .getDataObjects()
                        .get(0);

                    if(original.diff(merged).stream().anyMatch(a -> reference.values().contains(a)))
                    {
                        Dataset linkedData = dataReader
                        .readContractedData(
                            codeGenerator.generateSelectFromWhereCode(
                                null,
                                linkedTableSchema.getName(),
                                linkedConditions.getDataObjects(),
                                schema
                            ));

                        for(DataObject o: linkedData.getDataObjects())
                        {
                            cascadeUpdate(
                                linkedTableSchema,
                                o,
                                o.concat(parentMerge),
                                sqlScript);
                        }
                    }
                }
                else
                {
                    //Start the recursive second order merge 
                    mergeBagsInTable(
                        linkedTableSchema,
                        linkedDatasets,
                        parentMerge,
                        projectionKey,
                        sqlScript);
                }
            }
        }
        shiftLeft();
    }
    
    private void mergeBagsInTable(TableSchema tableSchema, List<Dataset> datasetsToMerge, DataObject parentMerge, Set<String> projectionKey, List<String> sqlScript) throws MergingException, SQLException, DataReadException
    {       
        if(datasetsToMerge.stream().allMatch(d -> d.getSize() == 0))
            return;
        
        //Do the second order merge
        Dataset mergedDataset = mergeStrategy
            .getSecondOrderHandler(tableSchema.getName())
            .merge(
                tableSchema,
                datasetsToMerge,
                projectionKey,
                parentMerge
            );
        
        Set<String> attributesInMerge = Stream
            .concat(
                projectionKey.stream(),
                parentMerge.getAttributes().stream()
            )
            .collect(Collectors.toSet());
        
        //Assemble originals that do not appear in the dataset:
        //These are staged for delete
        List<DataObject> stagedForDelete = datasetsToMerge
            .stream()
            .flatMap(d -> d.getDataObjects().stream())
            .filter(o -> mergedDataset
                    .getDataObjects()
                    .stream()
                    .noneMatch(mo -> o.concat(parentMerge).project(attributesInMerge).equals(mo)))
            .collect(Collectors.toList());
        
        //Proper deletion is done via cascade delete
        for(DataObject toDelete: stagedForDelete)
        {
            cascadeDelete(
                tableSchema,
                new SimpleDataset(Stream.of(toDelete).collect(Collectors.toList())),
                null,
                sqlScript
            );
        }
        
        //Assemble the new objects
        Dataset reducedMerge = new SimpleDataset();
        
        Map<DataObject, DataObject> changeMap = new HashMap<>();
            
        for(DataObject objectInMerge: mergedDataset.getDataObjects())
        {
            if(mergeStrategy
                .getSecondOrderHandler(tableSchema.getName())
                .isSecondOrderSubsumption()
            && mergedDataset.select(o -> !o.equals(objectInMerge)
            && objectInMerge.isSubsumedBy(o)).getSize() > 0)
                continue;
            
            //Collect originals
            Dataset originals = traceBackToOriginals(
                datasetsToMerge,
                tableSchema,
                objectInMerge,
                parentMerge,
                projectionKey);
            
            //If there is no merging of duplicate elements, just add any original
            if(!mergeStrategy
                .getSecondOrderHandler(tableSchema.getName())
                .isMergeDuplicateElements())
            {
                for(DataObject original: originals)
                {
                    reducedMerge.addDataObject(original);
                    changeMap.put(original, new DataObject(original));
                }
            }
            //Else, merge elements that link back to the same original
            else
            {
                switch(originals.getSize())
                {
                    case 0:
                        throw new MergingException("Error during second order merge: object " + objectInMerge + " cannot be traced to an original");
                    case 1:
                        DataObject original = originals
                            .getDataObjects()
                            .get(0);
                        //There is only one matching original
                        reducedMerge.addDataObject(original);

                        changeMap.put(original, new DataObject(original));
                        break;
                    default:
                        shiftRight();
                        DataObject merge = mergeRowsInTable(
                            tableSchema,
                            originals,
                            mergeStrategy
                            .getSecondOrderHandler(tableSchema.getName())
                            .isPushParentMerges()
                                ? parentMerge       //Push the parent merge
                                : new DataObject(),  //Don't push the parent merge yet!!
                            sqlScript);
                        reducedMerge.addDataObject(merge);
                        changeMap.put(merge, new DataObject(merge));
                        shiftLeft();
                }
            }
        }
        
        if(manualMerging)
        {
            print("Reduced merge has " + mergedDataset.getSize() + " objects");
        
            int idx = 1;
            for(DataObject o: reducedMerge.getDataObjects())
                print("" + (idx++) + ". " + o.toString());
        
            print("Are there any tuples that must be merged here? Yes or No");
            
            while(ConsoleFactory
                .getConsole()
                .readOptions("Yes", "No", "Y", "N")
                .toLowerCase()
                .startsWith("y"))
            {
                print("Enter a comma-separated list of indices");
                boolean validInput = false;
                
                String indices="";
                while(!validInput)
                {
                    indices = ConsoleFactory
                        .getConsole()
                        .readLine();
                    
                    int n = reducedMerge.getSize();
                    validInput =
                        !indices.isEmpty() &&
                        Stream
                            .of(indices.split(","))
                            .map(s->s.trim())
                            .allMatch(s -> 
                                s.matches("\\d+") && Integer.parseInt(s) >= 1 && Integer.parseInt(s) <= n);
                    if(!validInput)
                        print("Invalid input: provide a list of comma-separated indices between 1 and " + n, Console.ANSI_RED);
                }
                Set<Integer> indicesToMerge = Stream
                    .of(indices.split(","))
                    .map(s->s.trim())
                    .map(s -> Integer.parseInt(s)-1)    
                    .collect(Collectors.toSet());
                
                List<DataObject> objectsToMerge = new ArrayList<>();
                for(Integer index: indicesToMerge)
                {
                    objectsToMerge.add(reducedMerge.getDataObjects().get(index));
                }
                shiftRight();
                DataObject merge = mergeRowsInTable(
                    tableSchema,
                    new SimpleDataset(objectsToMerge),
                    mergeStrategy
                    .getSecondOrderHandler(tableSchema.getName())
                    .isPushParentMerges()
                        ? parentMerge       //Push the parent merge
                        : new DataObject(),  //Don't push the parent merge yet!!
                    sqlScript);
                shiftLeft();
                
                reducedMerge
                    .getDataObjects()
                    .removeAll(objectsToMerge);

                reducedMerge.addDataObject(merge);
                objectsToMerge
                    .stream()
                    .filter(o -> !o.equals(merge))
                    .forEach(o -> changeMap.entrySet().removeIf(e -> e.getValue().equals(o)));
                
                idx = 1;
                for(DataObject o: reducedMerge.getDataObjects())
                    print("" + (idx++) + ". " + o.toString());

                print("Are there any tuples that must be merged here? Yes or No");
                
            }
        }
        
        List<DataObject> originals = reducedMerge
            .getDataObjects()
            .stream()
            .collect(Collectors.toList());
        
        //Apply the parent merge to the objects in the reduced merge
        for(DataObject o: originals)
        {
            reducedMerge.replaceDataObject(o, o.concat(parentMerge));
            changeMap.put(o, changeMap.get(o).concat(parentMerge));
        }
        
        print("Reduced merge has " + mergedDataset.getSize() + " objects");
        
        for(DataObject o: reducedMerge.getDataObjects())
            print(o.toString());

        printVerbose("Validating reduced merge");
        
        Violation next = mergeValidator.validateSecondOrderMerge(
            tableSchema,
            datasetsToMerge,
            parentMerge,
            reducedMerge);

        boolean printFinal = !next.isVoid();
        
        while(!next.isVoid())
        {
            //Report
            print(next.toString(), Console.ANSI_RED);
            print("Violating object(s)", Console.ANSI_RED);
            int idx =1;
            for(DataObject violating: next.getViolatingObjects().getDataObjects())
            {
                print("-> " +(idx++) + ". " + violating, Console.ANSI_RED);
            }
            
            //If there is more than one object
            if(next.getViolatingObjects().getSize() > 1)
            {
                //Get input w.r.t. the fix strategy
                String choice = fixStrategy(tableSchema, next, parentMerge);
                
                if(choice.equals("delete") || choice.equals("d"))
                {
                    DataObject resolvedObject = deleteRowsInTable(
                        tableSchema,
                        next.getViolatingObjects(),
                        sqlScript);

                    reducedMerge
                        .getDataObjects()
                        .removeAll(next.getViolatingObjects().getDataObjects());
                    
                    if(resolvedObject != null)
                        reducedMerge.addDataObject(resolvedObject);
                    
                    //Each violating object that is different from the reduced object
                    //should be removed in the local change map
                    next
                        .getViolatingObjects()
                        .getDataObjects()
                        .stream()
                        .filter(o -> !o.equals(resolvedObject))
                        .forEach(o -> changeMap.entrySet().removeIf(e -> e.getValue().equals(o)));
                    
                }
                else
                {
                    reducedMerge = resolve(
                        tableSchema,
                        reducedMerge,
                        parentMerge,
                        next,
                        changeMap);
                }   
            }
            else
            {
                reducedMerge = resolve(
                    tableSchema,
                    reducedMerge,
                    parentMerge,
                    next,
                    changeMap);
            }

            //Re validate
            next = mergeValidator.validateSecondOrderMerge(
                tableSchema,
                datasetsToMerge,
                parentMerge,
                reducedMerge);
        }

        if(printFinal)
        {
            print("Final merge", Console.ANSI_GREEN);

            for(DataObject o: reducedMerge.getDataObjects())
                print(o.toString(), Console.ANSI_GREEN);
        }
        else
        {
            print("Merge is valid.", Console.ANSI_GREEN);
        }
        
        for(DataObject original: changeMap.keySet())
        {
            DataObject changed = changeMap.get(original);
            
            if(original.equals(changed))
                continue;
            
            //Update and cascade change to the key if necessary
            cascadeUpdate(
                tableSchema,
                original,
                changed,
                sqlScript);
        }
    }

    private Dataset resolve(TableSchema tableSchema, Dataset reducedMerge, DataObject parentMerge, Violation next, Map<DataObject, DataObject> changeMap)
    {
        Dataset fixedMerge = conflictResolver.fixSecondOrderViolation(
            tableSchema,
            reducedMerge,
            parentMerge,
            next);
                
        for(DataObject original: changeMap.keySet())
        {
            int index = reducedMerge
                .getDataObjects()
                .indexOf(changeMap.get(original));

            changeMap.put(
                original,
                fixedMerge.getDataObjects().get(index)
            );
        }
        return fixedMerge;
    }
    
    private Set<String> constructProjectionKey(Reference propagationReference, TableSchema propagationTableSchema)
    {
        if(this.mergeStrategy
            .getSecondOrderHandler(propagationTableSchema.getName())
            .isForeignKeyProject()
        && propagationTableSchema.getReferences().size() > 1)
        {
            return propagationTableSchema
                .getReferences()
                .stream()
                .filter(r -> !r.equals(propagationReference))
                .flatMap(r -> r.keySet().stream())
                .collect(Collectors.toSet());
        }
        
        //Return primary key
        return propagationTableSchema.getPrimaryKey();
    }
    
    private String fixStrategy(TableSchema tableSchema, Violation v, DataObject parentMerge) throws MergingException, SQLException, DataReadException
    {
        Set<String> unique = v.getViolatingAttributes();

        boolean canEdit = unique
        .stream()
        .allMatch(a ->
            parentMerge.getAttributes().contains(a)
            || !tableSchema.isReferencingAttribute(a));
        
        print("Choose a resolution strategy:");
        //print("- merge (m)");
        print("- delete (d)");
        
        if(canEdit)
            print("- update (u)");
        
        return canEdit
            ? ConsoleFactory.getConsole()
                .readOptions("update", "u", "delete", "d")
            : ConsoleFactory.getConsole()
                .readOptions("delete", "d");

    }

    private DataObject deleteRowsInTable(TableSchema tableSchema, Dataset objectsToMerge, List<String> sqlScript) throws MergingException, SQLException, DataReadException
    {
        ConsoleFactory
        .getConsole()
        .println("Use /n to pass the index of an object to keep. "
            + "Starting index is 1."
            + "Use /0 to delete all involved objects.");
        
        String toKeep = ConsoleFactory
        .getConsole()
        .readOptions(
            IntStream
            .rangeClosed(0, objectsToMerge.getSize())
            .mapToObj(i -> "/" + i)
            .collect(Collectors.toList())
            .toArray(new String[objectsToMerge.getSize()])
        );
        
        int index = Integer.parseInt(toKeep.substring(1)) - 1;
        
        DataObject merged = index == -1
            ? null
            : objectsToMerge.getDataObjects().get(index);

        cascadeDelete(
            tableSchema,
            objectsToMerge,
            merged,
            sqlScript);
        
        return merged;
  
    }
    
    private void cascadeDelete(TableSchema tableSchema, Dataset deleted, DataObject toKeep, List<String> sqlScript) throws SQLException, DataReadException, MergingException
    {
        print("Cascade delete to table " + tableSchema.getName(), Console.ANSI_BRIGHT_BLUE);
        //Inspect the database schema for referencing relations
        for(String linkedTableName: databaseSchema.keySet())
        {
            //Fetch table schema
            TableSchema linkedTableSchema = databaseSchema.get(linkedTableName);

            //Check
            if(linkedTableSchema.referencesTo(tableSchema))
                startPropagation(
                    tableSchema,
                    linkedTableSchema,
                    deleted,
                    toKeep,
                    sqlScript);
        }
        
        //Add the cleaning part
        codeGenerator.firstOrderSqlClean(
            deleted,
            toKeep,
            tableSchema,
            sqlScript,
            mergeValidator.getChangeLog(),
            schema
        );
    }
    
    private void cascadeUpdate(TableSchema tableSchema, DataObject original, DataObject changed, List<String> sqlScript) throws SQLException, DataReadException, MergingException
    {
        print("Cascade update to table " + tableSchema.getName(), Console.ANSI_BRIGHT_BLUE);
        //Inspect the database schema for referencing relations
        for(String linkedTableName: databaseSchema.keySet())
        {
            //Fetch table schema
            TableSchema linkedTableSchema = databaseSchema.get(linkedTableName);

            //Check
            if(linkedTableSchema.referencesTo(tableSchema))
                startPropagation(
                    tableSchema,
                    linkedTableSchema,
                    new SimpleDataset(Stream.of(original).collect(Collectors.toList())),
                    changed,
                    sqlScript);
        }
        
        //Add update statements
        sqlScript.add(
            codeGenerator.generateUpdateCode(
                changed.project(original.diff(changed)),
                tableSchema.getName(),
                original.project(tableSchema.getPrimaryKey()),
                schema
            )
        );
    }
    
    private boolean isCascadeDelete(TableSchema tableSchema, DataObject parentMerge)
    {
        return !parentMerge.getAttributes().isEmpty()
        && mergeStrategy.getSecondOrderHandler(tableSchema.getName()) != null
        && mergeStrategy
            .getSecondOrderHandler(tableSchema.getName())
            .getMerger() instanceof CascadeMerger;
    }
    
    private Dataset traceBackToOriginals(List<Dataset> datasetsToMerge, TableSchema tableSchema, DataObject objectInMerge, DataObject parentMerge, Set<String> projectionKey)
    {
        return new SimpleDataset(
        datasetsToMerge
        .stream()
        .map(dataset -> dataset.select(o ->
                o.project(projectionKey)
                .concat(parentMerge)
                .equals(
                    objectInMerge.project(projectionKey)
                    .concat(parentMerge)
                )
                || (mergeStrategy
                    .getSecondOrderHandler(tableSchema.getName())
                    .isSecondOrderSubsumption()
                    && o.project(projectionKey)
                        .concat(parentMerge)
                        .isSubsumedBy(
                            objectInMerge.project(projectionKey)
                            .concat(parentMerge)))
        ))
        .flatMap(selection -> selection.getDataObjects().stream())
        .collect(Collectors.toList()));
    }
    
    private void print(String message, String color)
    {
        ConsoleFactory
        .getConsole()
        .println(
            message,
            color
        );
    }
    
    private void printVerbose(String message)
    {
        ConsoleFactory
        .getConsole()
        .printlnVerbose(
            message
        );
    }
    
    private void print(String message)
    {
        ConsoleFactory
        .getConsole()
        .println(
            message
        );
    }
    
    private void shiftRight()
    {
        ConsoleFactory
        .getConsole()
        .increaseDepth();
    }
    
    private void shiftLeft()
    {
        ConsoleFactory
        .getConsole()
        .decreaseDepth();
    }
}
