package be.ugent.ledc.sqlmerge.propagation;

import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.sqlmerge.MergingException;
import be.ugent.ledc.sqlmerge.SecondOrderMerger;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class SecondOrderHandler
{
    private final SecondOrderMerger merger;
    
    /**
     * A flag to indicate that the union of foreign keys, minus the foreign key
     * that initiates the propagation, should be used as projection key.
     */
    private final boolean foreignKeyProject;
    
    /**
     * A flag to indicate that during second order merging, subsumed tuples should
     * be considered as redundant. Turning this flag on is useful when foreignKeyProject
     * is true and there is a desire to consider null values in foreign keys as undesirable.
     */
    private final boolean secondOrderSubsumption;
    
    /**
     * Force push parent merges. Setting this flag to true implies that parent merges
     * are always immediately applied in recursive merges. This allows more automated decisions,
     * but might be less flexible when unique constraints are in place in tables where second order merging is performed.
     */
    private final boolean pushParentMerges;
    
    /**
     * Forces merges of duplicate elements after the second order merge. When true,
     * no duplicate elements are allowed in the bag obtained after second order merging.
     * True by default. Set this flag to false if the foreign key values are not unique.
     */
    private final boolean mergeDuplicateElements;

    public SecondOrderHandler(SecondOrderMerger merger, boolean foreignKeyProject, boolean secondOrderSubsumption, boolean pushParentMerges, boolean mergeDuplicateElements)
    {
        this.merger = merger;
        this.foreignKeyProject = foreignKeyProject;
        this.secondOrderSubsumption = secondOrderSubsumption;
        this.pushParentMerges = pushParentMerges;
        this.mergeDuplicateElements = mergeDuplicateElements;
    }
    
    public SecondOrderHandler(SecondOrderMerger merger)
    {
        this(merger, false, false, true, true);
    }

    public Dataset merge(TableSchema tableSchema, List<Dataset> datasetsToMerge, Set<String> projectionKey, DataObject parentMerge) throws MergingException
    {
        ConsoleFactory
        .getConsole()
        .println("Merging bags in table " + tableSchema.getName());
        
        ConsoleFactory
        .getConsole()
        .println("Bags to merge:");
        
        int count = 0;
        
        for(Dataset dataset: datasetsToMerge)
        {
            ConsoleFactory
            .getConsole()
            .println("Bag " + (++count) + " (size " + dataset.getSize() + ") " +
                (dataset.getSize() == 0
                    ? ""
                    : dataset
                        .project(parentMerge.getAttributes())
                        .distinct()
                        .getDataObjects()
                        .get(0)
                ),
            Console.ANSI_CYAN);
            
            for(DataObject o: dataset.getDataObjects())
                ConsoleFactory
                .getConsole()
                .println("-> " + o.toString(), Console.ANSI_CYAN);
            
            ConsoleFactory
            .getConsole()
            .println("");
        }
        
        //Project the datasets over the projection key and merge
        Dataset mergedDataset = merger.merge(
            datasetsToMerge
                .stream()
                .map(dataset -> dataset.project(projectionKey))
                .collect(Collectors.toList()),
            parentMerge,
            mergeDuplicateElements);
        
        ConsoleFactory
            .getConsole()
            .printlnVerbose("Second order merge has " + mergedDataset.getSize() + " objects");
        
        for(DataObject o: mergedDataset.getDataObjects())
            ConsoleFactory
            .getConsole()
            .printlnVerbose(o.toString());
        

        return mergedDataset;
    }    

    public SecondOrderMerger getMerger()
    {
        return merger;
    }

    public boolean isForeignKeyProject()
    {
        return foreignKeyProject;
    }

    public boolean isSecondOrderSubsumption()
    {
        return secondOrderSubsumption;
    }

    public boolean isPushParentMerges()
    {
        return pushParentMerges;
    }

    public boolean isMergeDuplicateElements()
    {
        return mergeDuplicateElements;
    }
}
