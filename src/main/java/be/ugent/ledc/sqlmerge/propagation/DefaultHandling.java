package be.ugent.ledc.sqlmerge.propagation;

public interface DefaultHandling
{
    public FirstOrderHandler createDefaultFirstOrderHandler();

    public SecondOrderHandler createDefaultSecondOrderHandler();
    
}
