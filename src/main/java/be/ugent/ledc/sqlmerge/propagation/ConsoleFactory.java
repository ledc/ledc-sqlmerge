package be.ugent.ledc.sqlmerge.propagation;

public class ConsoleFactory
{
    private static final Console PRINTER = new Console();
    
    public static Console getConsole()
    {
        return PRINTER;
    }
}
