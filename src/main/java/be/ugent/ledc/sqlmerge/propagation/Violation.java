package be.ugent.ledc.sqlmerge.propagation;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Violation
{
    private final Dataset violatingObjects;
    
    private final Set<String> violatingAttributes;
    
    private final ViolationType violationType;
    
    public Violation(Dataset violatingObjects, Set<String> violatingAttributes, ViolationType violationType)
    {
        this.violatingObjects = violatingObjects;
        this.violatingAttributes = violatingAttributes;
        this.violationType = violationType;
    }
    
    public Violation(DataObject violatingObject, Set<String> violatingAttributes, ViolationType violationType)
    {
        this(new SimpleDataset(Stream.of(violatingObject).collect(Collectors.toList())), violatingAttributes, violationType);
    }
    
    public Violation(ViolationType violationType)
    {
        this(new SimpleDataset(), new HashSet<>(), violationType);
    }

    public Dataset getViolatingObjects()
    {
        return violatingObjects;
    }

    public Set<String> getViolatingAttributes()
    {
        return violatingAttributes;
    }

    public ViolationType getViolationType()
    {
        return violationType;
    }
    
    public boolean isVoid()
    {
        return
        violatingObjects == null
        || violatingObjects.getSize() == 0 
        || violatingAttributes == null
        || violatingAttributes.isEmpty();
    }
    
    @Override
    public String toString()
    {
        return isVoid()
            ? "No violation"
            : "Violation of " + violationType.name() + " constraint on attributes '" + this.violatingAttributes + "'";
    }
}
