package be.ugent.ledc.sqlmerge.propagation;

import be.ugent.ledc.core.binding.BindingException;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.binding.jdbc.JDBCBinder;
import be.ugent.ledc.core.binding.jdbc.JDBCDataReader;
import be.ugent.ledc.core.binding.jdbc.agents.DMLGenerator;
import be.ugent.ledc.core.binding.jdbc.schema.CheckConstraint;
import be.ugent.ledc.core.binding.jdbc.schema.Reference;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MergeValidator
{
    private final ChangeLog changeLog;
    
    private final JDBCDataReader reader;
    
    private final PropagationSQLGenerator codeGenerator;

    private final String schema;
    
    public MergeValidator(JDBCBinder binder) throws BindingException
    {
        this.changeLog = new ChangeLog(binder);
        this.reader = new JDBCDataReader(binder);
        this.codeGenerator = new PropagationSQLGenerator(
            binder
                .getRdb()
                .getVendor()
                .getAgent()
                .escaping()
        );
        this.schema = binder
            .getRdb()
            .getSchemaName();
    }
    
    public Violation validateFirstOrderMerge(TableSchema tableSchema, Dataset objectsToMerge, DataObject parentMerge, DataObject merged) throws DataReadException
    {
        //Verify primary key preservation
        Violation primaryKeyPreservation = verifyPrimaryKeyPreservation(
            tableSchema,
            objectsToMerge,
            parentMerge,
            merged);
        
        if(!primaryKeyPreservation.isVoid())
            return primaryKeyPreservation;
        
        //Verify check constraints
        Violation checks = verifyCheckConstraints(tableSchema, merged);
        
        if(!checks.isVoid())
            return checks;
        
        //Verify not null constraints
        Violation nulls = verifyNotNullConstraints(tableSchema, merged);
        
        if(!nulls.isVoid())
            return nulls;
        
        //Verify foreign key constraints
        Violation foreignKeys = verifyForeignKeyConstraints(tableSchema, merged);
        
        if(!foreignKeys.isVoid())
            return foreignKeys;
        
        //Verify unique constraints
        Violation unique = verifyUniqueConstraints(tableSchema, objectsToMerge, merged);
        
        if(!unique.isVoid())
            return unique;
        
        return new Violation(ViolationType.ANY);
    }
    
    public Violation validateSecondOrderMerge(TableSchema tableSchema, List<Dataset> datasetsToMerge, DataObject parentMerge, Dataset merged) throws DataReadException
    {
        Dataset allObjects = new SimpleDataset(datasetsToMerge
            .stream()
            .flatMap(d -> d.getDataObjects().stream())
            .collect(Collectors.toList()));
        
        for(DataObject mergedObject: merged.getDataObjects())
        {   
            //Verify primary key preservation
            Violation primaryKeyPreservation = verifyPrimaryKeyPreservation(
                tableSchema,
                allObjects,
                parentMerge,
                mergedObject);

            if(!primaryKeyPreservation.isVoid())
                return primaryKeyPreservation;
            
            //Verify check constraints
            Violation checks = verifyCheckConstraints(tableSchema, mergedObject);
        
            if(!checks.isVoid())
                return checks;
            
            //Verify not null constraints
            Violation nulls = verifyNotNullConstraints(tableSchema, mergedObject);

            if(!nulls.isVoid())
                return nulls;
            
            //Verify foreign key constraints
            Violation foreignKeys = verifyForeignKeyConstraints(
                tableSchema,
                mergedObject);

            if(!foreignKeys.isVoid())
                return foreignKeys;
        }

        
        //Verify unique constraints
        Violation unique = verifyUniqueConstraints(
            tableSchema,
            allObjects,
            merged);
        
        if(!unique.isVoid())
            return unique;

        
        return new Violation(ViolationType.ANY);
    }
    
    private Violation verifyPrimaryKeyPreservation(TableSchema tableSchema, Dataset objectsToMerge, DataObject parentMerge, DataObject merged) throws DataReadException
    {
        Set<String> tableKey = tableSchema.getPrimaryKey();
        
        boolean isKeyPreserved = objectsToMerge
            .getDataObjects()
            .stream()
            .anyMatch(o -> merged
                .project(tableKey)
                .isProjectionOf(o.concat(parentMerge)));
        
        return isKeyPreserved
            ? new Violation(ViolationType.PRIMARY_KEY_PRESERVATION)
            : new Violation(merged, tableKey, ViolationType.PRIMARY_KEY_PRESERVATION);
        
    }
    
    private Violation verifyCheckConstraints(TableSchema tableSchema, DataObject merged) throws DataReadException
    {
        //Test all relevant check constraints
        //Verify check constraints
        List<CheckConstraint> toTest = tableSchema
            .getCheckConstraints()
            .stream()
            .collect(Collectors.toList());
        
        Function<String, String> escape = reader
            .getBinder()
            .getRdb()
            .getVendor()
            .getAgent()
            .escaping();
        
        for(CheckConstraint cc: toTest)
        {
            String sqlTest = cc.getCheckClause();
            
            //Replace involved attributes by their values
            for(String a: cc.getInvolvedAttributes())
            {
                String stringValue = new DMLGenerator(escape)
                    .stringValue(merged.get(a), false);
                
                //We MUST cast null values explicitly to determine potential operator scope
                if(merged.get(a) == null)
                {
                    stringValue = "cast("
                        + stringValue
                        + " as "
                        + tableSchema.getAttributeProperty(a, TableSchema.PROP_DATATYPE)
                        +")";
                }
                
                sqlTest = sqlTest.replaceAll("(" + escape.apply(a) + "|" + a + ")", stringValue == null ? "null" : stringValue);
            }
            
            Boolean test = reader
                .readContractedData("select " + sqlTest + " as test;")
                .getDataObjects()
                .get(0)
                .getBoolean("test");
            
            //According to ternary logic of relational databases,
            // a test that returns null is due to a null value that
            // cannot be evaluated against the test
            // A check constraint that evaluates to NULL, will be allowed
            if(test != null && !test)
                return new CheckViolation(cc.getCheckClause(), merged, cc.getInvolvedAttributes());
        }
        
        return new CheckViolation();

    } 
    
    private Violation verifyUniqueConstraints(TableSchema tableSchema, Dataset objectsToMerge, DataObject merged) throws DataReadException
    {
        List<Set<String>> uniqueConstraintsSorted = sortUniqueConstraints(tableSchema);
        Set<String> tableKey = tableSchema.getPrimaryKey();
        String tableName = tableSchema.getName();
        
        for(Set<String> unique: uniqueConstraintsSorted)
        {
            Dataset allData = reader
                .readData(codeGenerator.generateSelectFromWhereCode(
                    null,
                    tableName,
                    merged.project(unique),
                    schema)
                );
            
            for(DataObject other: allData.getDataObjects())
            {
                if(other.equals(merged))
                    continue;
                
                //Attempt auto-fix
                if(objectsToMerge
                    .getDataObjects()
                    .stream()
                    .anyMatch(o -> o
                            .project(unique)
                            .equals(merged.project(unique)))
                )
                    continue;
                
                if(!changeLog.isDeleted(tableName, merged) || changeLog.isAdded(tableName, merged))
                    return new Violation(merged, unique, ViolationType.UNIQUE);
            }
        }
        
        return new Violation(ViolationType.UNIQUE);
    }
    
    private Violation verifyUniqueConstraints(TableSchema tableSchema, Dataset allObjects, Dataset merged) throws DataReadException
    {
        List<Set<String>> uniqueConstraintsSorted = sortUniqueConstraints(tableSchema);
        
        for(Set<String> unique: uniqueConstraintsSorted)
        {
            //Check for internal violations
            Dataset uniqueValues = merged.project(unique).distinct();
            
            if(uniqueValues.getSize() != merged.getSize())
            {
                for(DataObject value: uniqueValues.getDataObjects())
                {
                    Dataset conflictingValues = merged.select(o -> value.isProjectionOf(o));
                    
                    if(conflictingValues.getSize() > 1)
                        return new Violation(
                            conflictingValues,
                            unique,
                            ViolationType.UNIQUE);
                }
            }
        }
        
        //Check for external violations
        for(DataObject o: merged.getDataObjects())
        {
            Violation v = verifyUniqueConstraints(
                tableSchema,
                allObjects,
                o);
            
            if(!v.isVoid())
                return v;
        }
        
        return new Violation(ViolationType.UNIQUE);
    }
    
    private Violation verifyNotNullConstraints(TableSchema tableSchema, DataObject o) throws DataReadException
    {
        for(String a: tableSchema.getAttributeNames())
        {
            Object propertyValue = tableSchema.getAttributeProperty(a, TableSchema.PROP_IS_NULLABLE);
            
            if(propertyValue != null && !(Boolean)propertyValue && o.get(a) == null)
                return new Violation(o, Stream.of(a).collect(Collectors.toSet()), ViolationType.NOT_NULL);
        }
        
        return new Violation(ViolationType.NOT_NULL);
    }
    
    private Violation verifyForeignKeyConstraints(TableSchema tableSchema, DataObject merged) throws DataReadException
    {
        for(Reference r: tableSchema.getReferences())
        {
            Set<String> referencingAttributes = r.keySet();
            
            DataObject conditions = merged.project(r.keySet());
            
            //If one of the foreign key values is null, we skip the check.
            if(conditions.getAttributes().stream().anyMatch(a -> conditions.get(a) == null))
                continue;
            
            for(String a: r.keySet())
            {
                conditions.rename(a, r.get(a));
            }
            
            TableSchema referenced = r.getReferenced();

            Dataset referencedData =
                    reader.readData(
                        codeGenerator.generateSelectFromWhereCode(
                            new HashSet<>(r.values()),
                            referenced.getName(),
                            conditions,
                            schema)
            );
            
            //If any of the retrieved objects was not deleted
            //then we know the FK value still exists.
            if(referencedData.getDataObjects()
                .stream()
                .anyMatch(o -> !changeLog.isDeleted(referenced.getName(), o)))
            {
                continue;
            }
            
            if(!changeLog.isProjectionOfAdded(referenced.getName(), conditions))
                return new Violation(merged, referencingAttributes, ViolationType.FOREIGN_KEY_PRESERVATION);
        }
        
        return new Violation(ViolationType.FOREIGN_KEY_PRESERVATION);
    }

    /**
     * Sort unique constraints in such a way we will treat
     * violations involving foreign keys ONLY, first
     */
    private List<Set<String>> sortUniqueConstraints(TableSchema tableSchema)
    {
        return
            tableSchema
            .getUniqueConstraints()
            .values()
            .stream()
            .sorted(
                (u1,u2) ->
                    u1.stream().allMatch(a->tableSchema.isReferencingAttribute(a))
                        && u2.stream().anyMatch(a->!tableSchema.isReferencingAttribute(a))
                        ? -1
                        : u2.stream().allMatch(a->tableSchema.isReferencingAttribute(a))
                            && u1.stream().anyMatch(a->!tableSchema.isReferencingAttribute(a))
                            ? 1
                            : 0
        )
        .collect(Collectors.toList());
    }
    
    public ChangeLog getChangeLog()
    {
        return changeLog;
    }
}
