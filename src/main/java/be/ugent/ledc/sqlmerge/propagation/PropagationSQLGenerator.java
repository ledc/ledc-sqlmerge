package be.ugent.ledc.sqlmerge.propagation;

import be.ugent.ledc.core.binding.jdbc.agents.DMLGenerator;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.sqlmerge.MergingException;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PropagationSQLGenerator extends DMLGenerator
{    
    public PropagationSQLGenerator(Function<String,String> escape)
    {
        super(escape);
    }
    
    public void firstOrderSqlMerge(Dataset originals, DataObject mergedDataObject, TableSchema tableSchema, List<String> sqlScript, ChangeLog log, String schema) throws MergingException
    {
        if(mergedDataObject == null)
            return;
        
        //Iterate
        if(originals
            .getDataObjects()
            .stream()
            .anyMatch(o -> o.project(tableSchema.getPrimaryKey())
                .equals(mergedDataObject.project(tableSchema.getPrimaryKey())))
        )
        {
            DataObject fusedKey = mergedDataObject.project(tableSchema.getPrimaryKey());
            
            //Get the object that matches the fused key
            DataObject objectToChange = originals
                .getDataObjects()
                .stream()
                .filter(o -> o.project(tableSchema.getPrimaryKey()).equals(fusedKey))
                .findFirst()
                .orElseThrow(() -> new MergingException("Fusion error: key preservation is true but no object matches the fused key."));
            
            
            //Only do the update if there are effective changes to the object
            if(!objectToChange.diff(mergedDataObject).isEmpty())
                //Generate UPDATE statement
                sqlScript.add(generateUpdateCode(
                    mergedDataObject.project(mergedDataObject.diff(objectToChange)),
                    tableSchema.getName(),
                    fusedKey,
                    schema));
        }
        //Insert a new row
        else
        {
            //Generate INSERT statement
            sqlScript.add(
                generateInsertCode(
                    mergedDataObject,
                    tableSchema.getName(),
                    getEscape().apply(schema)));
        }
        
        log.addDataObject(tableSchema.getName(), mergedDataObject);
    }
    
    public void firstOrderSqlClean(Dataset originals, DataObject mergedDataObject, TableSchema tableSchema, List<String> sqlScript, ChangeLog log, String schema)
    {
        //Each original object should be removed, expect if it matches the primary key of the fused data object
        sqlScript.addAll(originals
            .getDataObjects()
            .stream()
            .filter(o -> mergedDataObject == null || !o.project(tableSchema.getPrimaryKey()).equals(mergedDataObject.project(tableSchema.getPrimaryKey())))
            .map(o -> generateDeleteCode(o, tableSchema, getEscape().apply(schema)))
            .collect(Collectors.toList()));
        
        //Mark objects as deleted, unless that object was kept.
        originals
            .getDataObjects()
            .stream()
            .filter(o -> !o.equals(mergedDataObject))
            .forEach(o -> log
                .deleteDataObject(tableSchema.getName(), o));
    }    
}
