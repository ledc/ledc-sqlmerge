package be.ugent.ledc.sqlmerge.propagation;

import be.ugent.ledc.sqlmerge.dataobject.DummyMerger;
import be.ugent.ledc.sqlmerge.secondorder.MultisetMerger;
import be.ugent.ledc.sqlmerge.secondorder.UnionMerger;
import java.util.HashMap;
import java.util.Map;

public class MergeStrategy
{
    /**
     * A mapping that maps table names to a handler for merges of rows in that table.
     */
    private final Map<String, FirstOrderHandler> firstOrderHandlers;
    
    /**
     * A mapping that maps table names to a handler for merges of bags of rows in that table.
     */
    private final Map<String, SecondOrderHandler> secondOrderHandlers;
    
    private final DefaultHandling defaultHandling;

    public MergeStrategy(Map<String, FirstOrderHandler> firstOrderHandlers, Map<String, SecondOrderHandler> secondOrderHandlers)
    {
        this.firstOrderHandlers = firstOrderHandlers;
        this.secondOrderHandlers = secondOrderHandlers;
        this.defaultHandling = new DefaultHandling()
        {
            @Override
            public FirstOrderHandler createDefaultFirstOrderHandler()
            {
                return new FirstOrderHandler(new DummyMerger());
            }

            @Override
            public SecondOrderHandler createDefaultSecondOrderHandler()
            {
                return new SecondOrderHandler(new MultisetMerger(new UnionMerger()));
            }
        };
    }

    public MergeStrategy()
    {
        this(new HashMap<>(), new HashMap<>());
    }

    public Map<String, FirstOrderHandler> getFirstOrderHandlers()
    {
        return firstOrderHandlers;
    }

    public Map<String, SecondOrderHandler> getSecondOrderHandlers()
    {
        return secondOrderHandlers;
    }
    
    public FirstOrderHandler getFirstOrderHandler(String tableName)
    {
        return firstOrderHandlers.get(tableName) == null
            ? defaultHandling.createDefaultFirstOrderHandler()
            : this.firstOrderHandlers.get(tableName);
    }
    
    public SecondOrderHandler getSecondOrderHandler(String tableName)
    {
        return this.secondOrderHandlers.get(tableName) == null
            ? defaultHandling.createDefaultSecondOrderHandler()
            : this.secondOrderHandlers.get(tableName);
    }

    public void addFirstOrderHandler(String tableName, FirstOrderHandler firstOrderHandler)
    {
        this.firstOrderHandlers.put(tableName, firstOrderHandler);
    }
    
    public void addSecondOrderHandler(String tableName, SecondOrderHandler secondOrderHandler)
    {
        this.secondOrderHandlers.put(tableName, secondOrderHandler);
    }
}
