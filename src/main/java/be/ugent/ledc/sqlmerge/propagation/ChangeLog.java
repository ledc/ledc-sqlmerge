package be.ugent.ledc.sqlmerge.propagation;

import be.ugent.ledc.core.binding.BindingException;
import be.ugent.ledc.core.binding.jdbc.JDBCBinder;
import be.ugent.ledc.core.binding.jdbc.schema.DatabaseSchema;
import be.ugent.ledc.core.dataset.DataObject;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ChangeLog
{
    private final Map<String, Set<DataObject>> dataAdded;
    
    private final Map<String, Set<DataObject>> dataDeleted;
    
    public ChangeLog(JDBCBinder binder) throws BindingException
    {
        DatabaseSchema schema = binder.getSchema();
        
        dataAdded = schema.getTableSchemas()
            .stream()
            .collect(Collectors.toMap(
                ts -> ts.getName(),
                ts -> new HashSet<>()
            ));
        
        dataDeleted = schema.getTableSchemas()
            .stream()
            .collect(Collectors.toMap(
                ts -> ts.getName(),
                ts -> new HashSet<>()
            ));
    }
    
    public void addDataObject(String tableName, DataObject o)
    {
        if(!dataAdded.containsKey(tableName)
        || !dataDeleted.containsKey(tableName))
        {
            throw new RuntimeException("Unknown table '" + tableName + "' in change log");
        }
        
        dataAdded.get(tableName).add(o);
        dataDeleted.get(tableName).remove(o);
    }
    
    public void deleteDataObject(String tableName, DataObject o)
    {
        if(!dataAdded.containsKey(tableName)
        || !dataDeleted.containsKey(tableName))
        {
            throw new RuntimeException("Unknown table '" + tableName + "' in change log");
        }
        
        dataDeleted.get(tableName).add(o);
        dataAdded.get(tableName).remove(o);
    }
    
    public boolean isDeleted(String tableName, DataObject o)
    {
        if(!dataDeleted.containsKey(tableName))
        {
            throw new RuntimeException("Unknown table '" + tableName + "' in change log");
        }
        
        return dataDeleted.get(tableName).contains(o);
    }
    
    public boolean isAdded(String tableName, DataObject o)
    {
        if(!dataAdded.containsKey(tableName))
        {
            throw new RuntimeException("Unknown table '" + tableName + "' in change log");
        }
        
        return dataAdded.get(tableName).contains(o);
    }
    
    public boolean isProjectionOfAdded(String tableName, DataObject projection)
    {
        if(!dataAdded.containsKey(tableName))
        {
            throw new RuntimeException("Unknown table '" + tableName + "' in change log");
        }
        
        //Check if any of the added objects has the requested projection
        return dataAdded
            .get(tableName)
            .stream()
            .anyMatch(o -> projection.isProjectionOf(o));
    }
}
