package be.ugent.ledc.sqlmerge.propagation;

import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.datastructures.Multiset;

public interface ConflictResolver
{
    public <I> Object resolve(TableSchema tableSchema, String attribute, Multiset<I> choices);
    
    public DataObject fixFirstOrderViolation(TableSchema tableSchema, Dataset objectsToMerge, DataObject merged, DataObject parentMerge, Violation v);
    
    public Dataset fixSecondOrderViolation(TableSchema tableSchema, Dataset merged, DataObject parentMerge, Violation v);
}
