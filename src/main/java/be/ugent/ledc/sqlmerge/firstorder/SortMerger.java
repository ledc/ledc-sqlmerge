package be.ugent.ledc.sqlmerge.firstorder;

import be.ugent.ledc.core.datastructures.Multiset;
import java.util.Comparator;
import java.util.stream.Collectors;
import be.ugent.ledc.sqlmerge.Merger;
import java.util.stream.IntStream;

public class SortMerger<T> implements Merger<T>
{        
    private final Comparator<T> comparator;
    
    private final Selector selector;

    public SortMerger(Comparator<T> comparator, Selector selector)
    {
        this.comparator = comparator;
        this.selector = selector;
    }

    public Comparator<T> getComparator()
    {
        return comparator;
    }

    public Selector getSelector()
    {
        return selector;
    }

    @Override
    public T merge(Multiset<T> sources)
    {
        return (T)selector.select(
            sources
            .keySet()
            .stream()
            .filter(t-> t != null)
            .flatMap(t -> IntStream
                .rangeClosed(1, sources.multiplicity(t))
                .mapToObj(i -> t)
            )
            .sorted(comparator)
            .collect(Collectors.toList())
        );

    }
}
