package be.ugent.ledc.sqlmerge.firstorder.dynamic;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface GeneralityEstimator<T>
{
    public Map<T, Integer> buildGeneralityMap(List<Set<T>> duplicateValues);
}
