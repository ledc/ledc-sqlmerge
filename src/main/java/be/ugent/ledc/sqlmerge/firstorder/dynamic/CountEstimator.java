package be.ugent.ledc.sqlmerge.firstorder.dynamic;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class CountEstimator<T> implements GeneralityEstimator<T>
{
    @Override
    public Map<T, Integer> buildGeneralityMap(List<Set<T>> duplicateValues)
    {
        Map<T, Integer> countMap = new HashMap<>();
        
        duplicateValues
            .stream()
            .flatMap(cluster -> cluster.stream())
            .forEach(value -> countMap.merge(value, 1, Integer::sum));
        
        return countMap;
    }
}
