package be.ugent.ledc.sqlmerge.firstorder.dynamic;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class RelationCountEstimator<T> implements GeneralityEstimator<T>
{
    @Override
    public Map<T, Integer> buildGeneralityMap(List<Set<T>> duplicateValues)
    {
        HashMap<T, Set<T>> peerMap = new HashMap<>();
        
        for(Set<T> cluster: duplicateValues)
        {
            for(T value1: cluster)
            {                   
                //Put
                if(!peerMap.containsKey(value1) )
                {
                    peerMap.put(value1, new HashSet<>());
                }
                
                for(T value2: cluster)
                {
                    peerMap.get(value1).add(value2);   
                }
            }
        }
        
        //Initialize
        HashMap<T, Integer> countMap = new HashMap<>();
        
        for(T value: peerMap.keySet())
        {
            countMap.put(value, peerMap.get(value).size());
        }
        
        //Return
        return countMap;
    }
    
}
