package be.ugent.ledc.sqlmerge.firstorder;

import be.ugent.ledc.core.datastructures.Multiset;
import be.ugent.ledc.core.util.ItemSelector;
import be.ugent.ledc.sqlmerge.Merger;

/**
 * Coallesce merging: select an item at random that is not null.
 * @author abronsel
 * @param <T> 
 */
public class CoallesceMerger<T> implements Merger<T>
{
    @Override
    public T merge(Multiset<T> input)
    {
        return ItemSelector.selectItemByCount(input);
    }
}
