package be.ugent.ledc.sqlmerge.firstorder;

import be.ugent.ledc.core.datastructures.Multiset;
import be.ugent.ledc.core.datastructures.relation.BinaryRelation;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import be.ugent.ledc.sqlmerge.Merger;
import be.ugent.ledc.sqlmerge.MergingException;
import be.ugent.ledc.sqlmerge.firstorder.dynamic.DynamicOrderConstructor;
import be.ugent.ledc.sqlmerge.firstorder.dynamic.GeneralityEstimator;
import java.util.List;
import java.util.Set;

public class BalancedMerger<T> implements Merger<T>
{
    private BinaryRelation<T> orderRelation;
    
    private Comparator<T> comparator;
    
    private int cutoff;

    public BalancedMerger(int cutoff)
    {
        this.cutoff = cutoff;
    }
    
    public BalancedMerger()
    {
        this(3);
    }

    public void learn(List<Set<T>> duplicateValues, GeneralityEstimator<T> generalityEstimator)
    {
        this.orderRelation = new DynamicOrderConstructor<>(cutoff, generalityEstimator)
            .makeBinaryRelation(duplicateValues);
        
        this.comparator = (value1, value2) ->
        {
            boolean b1 = orderRelation.contains(value1, value2);
            boolean b2 = orderRelation.contains(value2, value1);

            if (value1.equals(value2) || b1 == b2)
                return 0;
            return b1 ? 1 : -1;
        };
    }   
    
    @Override
    public T merge(Multiset<T> input) throws MergingException
    {
        if(orderRelation == null || comparator == null)
            throw new MergingException("Order relation must be learned first.");
        
        //Find all total values
        ArrayList<T> totalValues = new ArrayList<>();

        for (T object : input.keySet())
        {
            if (object != null)
            {
                boolean total = true;

                for (T alternative : input.keySet())
                {
                    if (!alternative.equals(object) && !orderRelation.contains(object, alternative) && !orderRelation.contains(alternative, object))
                    {
                        total = false;
                        break;
                    }
                }

                if (total)
                {
                    totalValues.add(object);
                }
            }
        }

        if (totalValues.isEmpty())
        {
            //Unspecified
            return null;
        }
        else
        {
            //Sort
            Collections.sort(totalValues, comparator);
            Collections.reverse(totalValues);

            //Select
            return totalValues.get(0);
        }
    }

    public int getCutoff()
    {
        return cutoff;
    }
}
