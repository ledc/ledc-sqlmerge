package be.ugent.ledc.sqlmerge.firstorder;

import java.util.List;
import java.util.function.Function;

public enum Selector
{
    FIRST(options -> options.isEmpty() ? null : options.get(0)),
    MIDDLE(options -> options.isEmpty()
        ? null
        : options.get(options.size()/2)),
    LAST(options -> options.isEmpty() ? null : options.get(options.size()-1)),
    ;
    
    private final Function<List,Object> embeddedSelector;

    private Selector(Function<List, Object> embeddedSelector)
    {
        this.embeddedSelector = embeddedSelector;
    }

    public Object select(List options)
    {
        return embeddedSelector.apply(options);
    }
    
}
