package be.ugent.ledc.sqlmerge.firstorder.dynamic;

import be.ugent.ledc.core.datastructures.Couple;
import be.ugent.ledc.core.datastructures.Multiset;
import be.ugent.ledc.core.datastructures.relation.BinaryRelation;
import be.ugent.ledc.sqlmerge.propagation.ConsoleFactory;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DynamicOrderConstructor<T>
{     
    private final int k;
    
    private final GeneralityEstimator<T> generalityEstimator;

    public DynamicOrderConstructor(int k, GeneralityEstimator<T> generalityEstimator)
    {
        this.k = k;
        this.generalityEstimator = generalityEstimator;
    }
    
    public BinaryRelation<T> makeBinaryRelation(List<Set<T>> duplicateValues)
    {
        //Construct Generative MultiRelation
        Multiset<Couple<T>> generativeMultiRelation = new Multiset<>();
        
        Map<T, Integer> countMap = this.generalityEstimator.buildGeneralityMap(duplicateValues);
        
        for(Set<T> cluster: duplicateValues)
        {
            List<T> clusterAsList = new ArrayList<>(cluster);
            
            for(int i=0; i<clusterAsList.size(); i++)
            {               
                //Get attribute
                T value1 = clusterAsList.get(i);
                
                for(int j=i; j<clusterAsList.size(); j++)
                {
                    T value2 = clusterAsList.get(j);
                    
                    if(value1 != null && value2 != null)
                    {
                        //Make couple
                        Couple c = new Couple(value1, value2);

                        //Add to multirelation
                        generativeMultiRelation.add(c, 1);
                    }
                }
            }
        }
        
        //Cut off
        Multiset<Couple<T>> kCutRelation = generativeMultiRelation.multiplicityCut(k);      
        
        //Make a Binary Relation from this cutted multirelation
        BinaryRelation<T> order = new BinaryRelation<>(kCutRelation
            .keySet()
            .stream()
            .flatMap(c -> Stream.of(c.getFirst(),c.getSecond()))
            .collect(Collectors.toSet())
        );
        
        for(Couple<T> c: kCutRelation.keySet())
        {
            T first = c.getFirst();
            T second = c.getSecond();
            
            //Get counts
            int countFirst = countMap.get(first);
            int countLast = countMap.get(second);
            
            if(countFirst < countLast)
            {
                order.add(first, second);
            }
            else if(countFirst > countLast)
            {
                order.add(second, first);
            }
        }
    
        //Check properties
        if(!order.isReflexive())
        {            
            order = order.reflexiveClosure();
        }
        
        if(!order.isAntisymmetric())
        {            
            List<T> values = order.elementStream().toList();
            
            //Make antisymmetric
            for(int i=0;i<values.size(); i++)
            {               
                T s1 = values.get(i);
                
                for(int j=i+1;j<values.size(); j++)
                {
                    T s2 = values.get(j);
                    
                    if(order.contains(s1,s2) && order.contains(s2,s1))
                    {
                        //Check frequency map
                        if(countMap.get(s1) > countMap.get(s2))
                            order.delete(s1,s2);
                        else
                           order.delete(s2,s1);
                    }
                }
            }
        }
        
        if(!order.isTransitive())
        {
            //System.out.println("Fixing transitivity");
            order = order.transitiveClosure();
        }
        
        ConsoleFactory
        .getConsole()
        .printlnVerbose("Partial order learning result (k=" + this.k + "):");
        
        order
        .stream()
        .map(c -> c.getFirst() + " specifies " + c.getSecond())
        .forEach(s -> ConsoleFactory.getConsole().printlnVerbose(s));
        
        return order;
    }
}
