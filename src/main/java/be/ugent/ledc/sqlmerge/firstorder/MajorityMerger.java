package be.ugent.ledc.sqlmerge.firstorder;

import be.ugent.ledc.core.datastructures.Multiset;
import be.ugent.ledc.sqlmerge.Merger;

/**
 * Perform the merge by selecting the input item with highest multiplicity.
 * If multiple such items exist, select one at random.
 * @author abronsel
 * @param <T> 
 */
public class MajorityMerger<T> implements Merger<T>
{
    @Override
    public T merge(Multiset<T> input)
    {
        //Maximum
        int maximalMultiplicity = input.maximum();

        return maximalMultiplicity == 0
            ? null
            : input
            .entrySet()
            .stream()
            .filter(e -> e.getValue() == maximalMultiplicity)
            .findAny()
            .get()
            .getKey();
    }
}
