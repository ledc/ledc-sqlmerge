package be.ugent.ledc.sqlmerge;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import java.util.List;

public interface SecondOrderMerger
{
    Dataset merge(List<Dataset> input, DataObject parentMerge, boolean mergeDuplicateElements) throws MergingException;
    
    public default Dataset applyParentMerge(Dataset dataset, DataObject parentMerge)
    {
        Dataset result = new SimpleDataset();
        
        for(DataObject o: dataset.getDataObjects())
        {
            result.addDataObject(new DataObject()
                .concat(o)
                .concat(parentMerge));
        }
        
        return result;
    }
}
