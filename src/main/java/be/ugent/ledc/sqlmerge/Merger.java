package be.ugent.ledc.sqlmerge;

import be.ugent.ledc.core.datastructures.Multiset;

public interface Merger<T>
{
    T merge(Multiset<T> input) throws MergingException;
}
