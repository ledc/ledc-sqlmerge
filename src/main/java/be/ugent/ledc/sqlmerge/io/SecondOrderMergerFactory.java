package be.ugent.ledc.sqlmerge.io;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.config.FeatureBuilder;
import be.ugent.ledc.core.config.FeatureConsumer;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.sqlmerge.propagation.SecondOrderHandler;
import be.ugent.ledc.sqlmerge.secondorder.CascadeMerger;
import be.ugent.ledc.sqlmerge.secondorder.IntersectionMerger;
import be.ugent.ledc.sqlmerge.secondorder.MaximalCoherentMerger;
import be.ugent.ledc.sqlmerge.secondorder.MultisetMerger;
import be.ugent.ledc.sqlmerge.secondorder.OptimalMerger;
import be.ugent.ledc.sqlmerge.secondorder.UnionMerger;

public class SecondOrderMergerFactory implements FeatureConsumer<SecondOrderHandler>, FeatureBuilder<SecondOrderHandler>
{
    public static final String TYPE = "mergerType";
    
    public static final String TYPE_CASCADE = "Cascade";
    public static final String TYPE_UNION   = "Union";
    public static final String TYPE_INTERSECTION = "Intersection";
    public static final String TYPE_OPTIMAL = "Optimal";
    public static final String TYPE_MCS     = "MaxCoherent";
    
    public static final String FOREIGN_KEY_PROJECT      = "foreignKeyProjection";
    public static final String SECOND_ORDER_SUBSUMPTION = "secondOrderSubsumption";
    public static final String PUSH_PARENT_MERGES       = "pushParentMerges";
    public static final String MERGE_DUPLICATE_ELEMENTS = "mergeDuplicateElements";
    
    @Override
    public SecondOrderHandler buildFromFeatures(FeatureMap fm) throws ParseException
    {
        if(fm.getString(TYPE) == null)
            throw new ParseException("Cannot build second order merger. Required feature '" + TYPE + "' is missing.");
        
        boolean fkProject = fm.getBoolean(FOREIGN_KEY_PROJECT) == null
            ? false
            : fm.getBoolean(FOREIGN_KEY_PROJECT);
        
        boolean soSubsumption = fm.getBoolean(SECOND_ORDER_SUBSUMPTION) == null
            ? false
            : fm.getBoolean(SECOND_ORDER_SUBSUMPTION);
        
        boolean pushParentMerge = fm.getBoolean(PUSH_PARENT_MERGES) == null
            ? true
            : fm.getBoolean(PUSH_PARENT_MERGES);
        
        boolean mergeDuplicateElements = fm.getBoolean(MERGE_DUPLICATE_ELEMENTS) == null
            ? true
            : fm.getBoolean(MERGE_DUPLICATE_ELEMENTS);
        
        if(fm.getString(TYPE).equals(TYPE_CASCADE))
            return new SecondOrderHandler(
                new CascadeMerger(),
                fkProject,
                soSubsumption,
                pushParentMerge,
                mergeDuplicateElements
            );
        if(fm.getString(TYPE).equals(TYPE_UNION))
            return new SecondOrderHandler(
                new MultisetMerger(new UnionMerger()),
                fkProject,
                soSubsumption,
                pushParentMerge,
                mergeDuplicateElements);
        if(fm.getString(TYPE).equals(TYPE_INTERSECTION))
            return new SecondOrderHandler(
                new MultisetMerger(new IntersectionMerger()),
                fkProject,
                soSubsumption,
                pushParentMerge,
                mergeDuplicateElements
            );
        if(fm.getString(TYPE).equals(TYPE_OPTIMAL))
            return new SecondOrderHandler(
                new MultisetMerger(new OptimalMerger()),
                fkProject,
                soSubsumption,
                pushParentMerge,
                mergeDuplicateElements
            );
        if(fm.getString(TYPE).equals(TYPE_MCS))
            return new SecondOrderHandler(
                new MultisetMerger(new MaximalCoherentMerger()),
                fkProject,
                soSubsumption,
                pushParentMerge,
                mergeDuplicateElements
            );
        
        throw new ParseException("Cannot build second order merger. Unsupported merger type: " + fm.getString(TYPE));
    }

    @Override
    public FeatureMap buildFeatureMap(SecondOrderHandler handler) throws ParseException
    {
        FeatureMap fm = new FeatureMap()
            .addFeature(FOREIGN_KEY_PROJECT, handler.isForeignKeyProject())
            .addFeature(SECOND_ORDER_SUBSUMPTION, handler.isSecondOrderSubsumption())
            .addFeature(PUSH_PARENT_MERGES, handler.isPushParentMerges())
            .addFeature(MERGE_DUPLICATE_ELEMENTS, handler.isMergeDuplicateElements());
        
        if(handler.getMerger() instanceof CascadeMerger)
            fm.addFeature(TYPE, TYPE_CASCADE);
        else if(handler.getMerger() instanceof MultisetMerger)
        {
            MultisetMerger mm = (MultisetMerger)handler.getMerger();
            
            if(mm.getEmbeddedMerger() instanceof UnionMerger)
                fm.addFeature(TYPE, TYPE_UNION);
            else if(mm.getEmbeddedMerger() instanceof IntersectionMerger)
                fm.addFeature(TYPE, TYPE_INTERSECTION);
            else if(mm.getEmbeddedMerger() instanceof OptimalMerger)
                fm.addFeature(TYPE, TYPE_OPTIMAL);
            else if(mm.getEmbeddedMerger() instanceof MaximalCoherentMerger)
                fm.addFeature(TYPE, TYPE_MCS);
            else
                throw new ParseException("Cannot convert to feature map. Unsupported second order merger type: " + handler.getClass());
        }
        else
            throw new ParseException("Cannot convert to feature map. Unsupported second order merger type: " + handler.getClass());

        return fm;
    }   
}