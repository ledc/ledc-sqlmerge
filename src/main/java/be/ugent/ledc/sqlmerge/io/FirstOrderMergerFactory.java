package be.ugent.ledc.sqlmerge.io;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.config.FeatureBuilder;
import be.ugent.ledc.core.config.FeatureConsumer;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.sqlmerge.Merger;
import be.ugent.ledc.sqlmerge.dataobject.HierarchicalDataObjectMerger;
import be.ugent.ledc.sqlmerge.dataobject.SortDataObjectMerger;
import be.ugent.ledc.sqlmerge.firstorder.BalancedMerger;
import be.ugent.ledc.sqlmerge.firstorder.CoallesceMerger;
import be.ugent.ledc.sqlmerge.firstorder.MajorityMerger;
import be.ugent.ledc.sqlmerge.firstorder.Selector;
import be.ugent.ledc.sqlmerge.firstorder.SortMerger;
import java.util.Comparator;
import java.util.Map.Entry;

public class FirstOrderMergerFactory implements FeatureConsumer<Merger>, FeatureBuilder<Merger>
{
    public static final String TYPE = "mergerType";
    
    public static final String TYPE_HIERARCHICAL    = "Hierarchical";
    public static final String TYPE_OBJECT_SORTED   = "SortSelect";
    public static final String TYPE_SORTED          = "Select";
    public static final String TYPE_MAJORITY        = "Majority";
    public static final String TYPE_COALLESCE       = "Coallesce";
//    public static final String TYPE_AGGREGATE       = "Aggregate";
    public static final String TYPE_BALANCED        = "Balanced";
    
    public static final String NESTED_MERGERS   = "NestedMergers";
    public static final String SORT_KEY         = "SortKey";
    public static final String SELECTOR         = "Selector";
    public static final String CUTOFF           = "Cutoff";
    
    @Override
    public Merger buildFromFeatures(FeatureMap fm) throws ParseException
    {
        if(fm.getString(TYPE) == null)
            throw new ParseException("Cannot build first order handler. Required feature '" + TYPE + "' is missing.");
        
        if(fm.getString(TYPE).equals(TYPE_HIERARCHICAL))
        {
            HierarchicalDataObjectMerger hdom = new HierarchicalDataObjectMerger();
            
            for(Entry<String, Object> e: fm.getFeatureMap(NESTED_MERGERS).getFeatures().entrySet())
            {
                hdom.addAttributeMerger(
                e.getKey(),
                buildFromFeatures((FeatureMap) e.getValue()));
            }
            return hdom;
        }
        if(fm.getString(TYPE).equals(TYPE_OBJECT_SORTED))
        {
            if(fm.getList(SORT_KEY) == null)
                throw new ParseException("Cannot build object sort merger. Required feature '" + SORT_KEY + "' is missing.");
            
            if(fm.getString(SELECTOR) == null)
                throw new ParseException("Cannot build object sort merger. Required feature '" + SELECTOR + "' is missing.");
            
            return new SortDataObjectMerger(
                fm.getList(SORT_KEY),
                Selector.valueOf(fm.getString(SELECTOR))
            );
        }
        if(fm.getString(TYPE).equals(TYPE_SORTED))
        {            
            if(fm.getString(SELECTOR) == null)
                throw new ParseException("Cannot build sort merger. Required feature '" + SELECTOR + "' is missing.");
            
            return new SortMerger(Comparator.naturalOrder(), Selector.valueOf(fm.getString(SELECTOR)));
        }
        if(fm.getString(TYPE).equals(TYPE_MAJORITY))
            return new MajorityMerger();
        if(fm.getString(TYPE).equals(TYPE_COALLESCE))
            return new CoallesceMerger();
        if(fm.getString(TYPE).equals(TYPE_BALANCED))
        {
            return fm.getInteger(CUTOFF) == null
                ? new BalancedMerger()
                : new BalancedMerger(fm.getInteger(CUTOFF));
        }
            
        throw new ParseException("Cannot build first order merger. Unsupported merger type: " + fm.getString(TYPE));
    }

    @Override
    public FeatureMap buildFeatureMap(Merger m) throws ParseException
    {
        FeatureMap fm = new FeatureMap();
        
        if(m instanceof HierarchicalDataObjectMerger)
        {
            fm.addFeature(TYPE, TYPE_HIERARCHICAL);

            FeatureMap nestedMergers = new FeatureMap();
            for(String a: ((HierarchicalDataObjectMerger)m).getAttributeMergers().keySet())
            {
                nestedMergers.addFeature(a, buildFeatureMap(((HierarchicalDataObjectMerger) m).getAttributeMergers().get(a)).getFeatures());
            }
            
            fm.addFeature(NESTED_MERGERS, nestedMergers.getFeatures());
        }
        else if(m instanceof SortDataObjectMerger)
        {
            fm.addFeature(TYPE, TYPE_OBJECT_SORTED);
            fm.addFeature(SORT_KEY, ((SortDataObjectMerger)m).getSortKey());
            fm.addFeature(SELECTOR, ((SortDataObjectMerger)m).getSelector().name());
        }
        else if(m instanceof SortMerger)
        {
            fm.addFeature(TYPE, TYPE_SORTED);
            fm.addFeature(SELECTOR, ((SortMerger)m).getSelector().name());
        }
        else if(m instanceof MajorityMerger)
            fm.addFeature(TYPE, TYPE_MAJORITY);
        else if(m instanceof CoallesceMerger)
            fm.addFeature(TYPE, TYPE_COALLESCE);
        else if(m instanceof BalancedMerger)
        {
            fm.addFeature(TYPE, TYPE_BALANCED);
            fm.addFeature(CUTOFF, ((BalancedMerger)m).getCutoff());
        }
        else
            throw new ParseException("Cannot convert to feature map. Unsupported first order merge type. " + m.getClass());
        
        return fm;
    }
    
}
