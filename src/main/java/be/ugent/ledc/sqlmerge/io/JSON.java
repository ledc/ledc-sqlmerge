package be.ugent.ledc.sqlmerge.io;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.binding.jdbc.JDBCBinder;
import be.ugent.ledc.core.binding.jdbc.JDBCBinderFactory;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.sqlmerge.propagation.MergeStrategy;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.json.JSONObject;
import org.json.JSONTokener;

public class JSON
{
    public static final String BINDER = "Binder";
    public static final String STRATEGY = "MergeStrategy";
    
    public static void dump(JDBCBinder binder, MergeStrategy mergeStrategy, File outputFile) throws FileNotFoundException, ParseException, IOException
    {
        BufferedWriter writer = new BufferedWriter(
            new OutputStreamWriter(
                new FileOutputStream(outputFile),
                StandardCharsets.UTF_8)
        );
        
        JSONObject mainObject = new JSONObject();
        
        mainObject.put(BINDER, new JSONObject(new JDBCBinderFactory()
            .buildFeatureMap(binder)
            .getFeatures())
        );
        
        mainObject.put(STRATEGY, new JSONObject(new MergeStrategyFactory()
            .buildFeatureMap(mergeStrategy)
            .getFeatures())
        );
        
        writer.write(mainObject.toString(4)); 
        writer.flush();
    }
    
    public static JDBCBinder readJDBCBinder(File inputFile) throws FileNotFoundException, ParseException
    {
        JSONObject topObject = new JSONObject(
            new JSONTokener(
                new BufferedReader(
                    new InputStreamReader(
                        new FileInputStream(inputFile),
                        StandardCharsets.UTF_8))));
        
        return new JDBCBinderFactory()
            .buildFromFeatures(
                convertToFeatureMap(topObject
                    .getJSONObject(BINDER)
                    .toMap()
                )
            );
    }
    
    public static MergeStrategy readMergeStrategy(File inputFile) throws FileNotFoundException, ParseException
    {
        JSONObject topObject = new JSONObject(
            new JSONTokener(
                new BufferedReader(
                    new InputStreamReader(
                        new FileInputStream(inputFile),
                        StandardCharsets.UTF_8))));
        
        return new MergeStrategyFactory()
            .buildFromFeatures(
                convertToFeatureMap(topObject
                    .getJSONObject(STRATEGY)
                    .toMap()
                )
            );
    }
    
    private static FeatureMap convertToFeatureMap(Map<String, Object> mapping)
    {
        FeatureMap fm = new FeatureMap();
        
        for(String key: mapping.keySet())
        {
            if(mapping.get(key) instanceof Map)
                fm.addFeature(key, convertToFeatureMap((Map) mapping.get(key)));
            else if (mapping.get(key) instanceof List)
                fm.addFeature(key, convertList((List) mapping.get(key)));
            else
                fm.addFeature(key, mapping.get(key));
        }
        
        return fm;
    }
    
    private static List<FeatureMap> convertList(List list)
    {
        List featureList = new ArrayList();
        
        for(int i=0; i<list.size(); i++)
        {
            if(list.get(i) instanceof Map)
                featureList.add(convertToFeatureMap((Map) list.get(i)));
            else if (list.get(i) instanceof List)
                featureList.add(convertList((List) list.get(i)));
            else
                featureList.add(list.get(i));
        }
        
        return featureList;
    }
}
