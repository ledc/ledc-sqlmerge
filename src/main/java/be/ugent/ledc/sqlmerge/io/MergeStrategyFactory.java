package be.ugent.ledc.sqlmerge.io;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.config.FeatureBuilder;
import be.ugent.ledc.core.config.FeatureConsumer;
import be.ugent.ledc.core.config.FeatureMap;
import be.ugent.ledc.sqlmerge.propagation.FirstOrderHandler;
import be.ugent.ledc.sqlmerge.propagation.MergeStrategy;
import be.ugent.ledc.sqlmerge.propagation.SecondOrderHandler;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MergeStrategyFactory implements FeatureBuilder<MergeStrategy>, FeatureConsumer<MergeStrategy>
{
    public static final String FIRST_ORDER_MERGERS      = "firstOrderMergers";
    public static final String SECOND_ORDER_MERGERS     = "secondOrderMergers";
    
    private final FirstOrderMergerFactory foFactory = new FirstOrderMergerFactory();
    private final SecondOrderMergerFactory soFactory = new SecondOrderMergerFactory();
    
    @Override
    public FeatureMap buildFeatureMap(MergeStrategy m) throws ParseException
    {
        FeatureMap fm = new FeatureMap();
        
        FeatureMap fo = new FeatureMap();

        for(String tableName: m.getFirstOrderHandlers().keySet())
        {
            fo.addFeature(tableName, foFactory
                .buildFeatureMap(
                    m.getFirstOrderHandler(tableName)
                    .getMerger()
                )
                .getFeatures()
            );
        }
        
        fm.addFeature(FIRST_ORDER_MERGERS, fo.getFeatures());
        
        FeatureMap so = new FeatureMap();

        for(String tableName: m.getSecondOrderHandlers().keySet())
        {
            so.addFeature(tableName, soFactory
                .buildFeatureMap(
                    m.getSecondOrderHandler(tableName))
                .getFeatures()
            );
        }
        
        fm.addFeature(SECOND_ORDER_MERGERS, so.getFeatures());
        
        return fm;
    }

    @Override
    public MergeStrategy buildFromFeatures(FeatureMap featureMap) throws ParseException
    {
        Map<String, FirstOrderHandler> fo = new HashMap<>();
        Map<String, SecondOrderHandler> so = new HashMap<>();
        
        if(featureMap.getFeatureMap(FIRST_ORDER_MERGERS) != null)
        {
            FeatureMap fom = featureMap.getFeatureMap(FIRST_ORDER_MERGERS);
            
            for(String tableName: fom.getFeatures().keySet())
            {
                fo.put(tableName, new FirstOrderHandler(foFactory.buildFromFeatures(fom.getFeatureMap(tableName))));
            }
        }
        
        if(featureMap.getFeatureMap(SECOND_ORDER_MERGERS) != null)
        {
            FeatureMap som = featureMap.getFeatureMap(SECOND_ORDER_MERGERS);
            
            for(String tableName: som.getFeatures().keySet())
            {
                so.put(tableName, soFactory.buildFromFeatures(som.getFeatureMap(tableName)));
            }
        }
        
        return new MergeStrategy(fo, so);
    }
    
}
