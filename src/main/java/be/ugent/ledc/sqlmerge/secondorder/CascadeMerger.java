package be.ugent.ledc.sqlmerge.secondorder;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.sqlmerge.SecondOrderMerger;
import java.util.List;

public class CascadeMerger implements SecondOrderMerger
{   
    @Override
    public Dataset merge(List<Dataset> input, DataObject parentMerge, boolean mergeDuplicateElements)
    {
        return input
            .stream()
            .filter(dataset -> dataset
                .getDataObjects()
                .stream()
                .allMatch(o -> parentMerge.isProjectionOf(o)))
            .findFirst()
            .orElse(new SimpleDataset());
        
    }
}
