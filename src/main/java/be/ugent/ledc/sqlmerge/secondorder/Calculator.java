package be.ugent.ledc.sqlmerge.secondorder;

import be.ugent.ledc.core.datastructures.Multiset;
import be.ugent.ledc.sqlmerge.secondorder.UnionMerger;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.function.BinaryOperator;

public class Calculator
{
    public static <U> float localRecall(Multiset<Multiset<U>> sources, int multiplicity, U u)
    {
        //Initialize locall recall
        float localRecall = 0.0f;

        //Get the cardinality of the sources
        int numOfSources = sources.cardinality();

        //Iterate over sources
        Iterator<Multiset<U>> sourceIterator = sources.keySet().iterator();

        while(sourceIterator.hasNext())
        {
            //Get the next source
            Multiset<U> source = sourceIterator.next();

            //Get source multiplicity
            int sourceMultiplicity = sources.multiplicity(source);

            if(source.multiplicity(u) <= multiplicity)
            {
                localRecall += sourceMultiplicity;
            }
        }

        //Normalize
        localRecall /= numOfSources;

        return localRecall;
    }

    public static <U> float localPrecision(Multiset<Multiset<U>> sources, int multiplicity, U u)
    {
        //Initialize locall precision
        float localPrecision = 0.0f;

        //Get the cardinality of the sources
        int numOfSources = sources.cardinality();

        //Iterate over sources
        Iterator<Multiset<U>> sourceIterator = sources.keySet().iterator();

        while(sourceIterator.hasNext())
        {
            //Get the next source
            Multiset<U> source = sourceIterator.next();

            //Get source multiplicity
            int sourceMultiplicity = sources.multiplicity(source);

            if(source.multiplicity(u) >= multiplicity)
            {
                localPrecision += sourceMultiplicity;
            }
        }

        //Normalize
        localPrecision /= numOfSources;

        return localPrecision;
    }

    public static <U> double recall(Multiset<Multiset<U>> sources, Multiset<U> solution, BinaryOperator<Float> tnorm)
    {
        //Construct upper solution
        Multiset<U> upperSolution = new Multiset<>();

        for(Multiset<U> source: sources.keySet())
        {
            //Union
            upperSolution = upperSolution.union(source);
        }
        
        if(upperSolution.cardinality() == 0)
        {
            return 1.0;
        }

        //Initialize a collection to store locall recalls
        List<Float> localRecallList = new ArrayList<>();

        for(U u: upperSolution.keySet())
        {
            //Calculate locall recall
            Float localRecall = localRecall(sources, solution.multiplicity(u), u);

            //Add to collection
            localRecallList.add(localRecall);
        }

        //Calculate recall
        Float recall = localRecallList.stream().reduce(1f, tnorm);

        //Return
        return recall;
    }

    public static <U> double precision(Multiset<Multiset<U>> sources, Multiset<U> solution, BinaryOperator<Float> tnorm)
    {
        //Construct upper solution
        Multiset<U> upperSolution = new Multiset<>();

        for(Multiset<U> source: sources.keySet())
        {
            //Union
            upperSolution = upperSolution.union(source);
        }
        
        if(upperSolution.cardinality() == 0)
        {
            return solution.cardinality() == 0 ? 1.0 : 0.0;
        }
        
        //Initialize a collection to store locall recalls
        List<Float> localPrecisionList = new ArrayList<>();

        for(U u: upperSolution.keySet())
        {
            //Calculate local precision
            Float localPrecision = localPrecision(sources, solution.multiplicity(u), u);

            //Add to collection
            localPrecisionList.add(localPrecision);
        }

        //Calculate precision
        double precision = localPrecisionList.stream().reduce(1f, tnorm);

        //Return
        return precision;
    }
}

