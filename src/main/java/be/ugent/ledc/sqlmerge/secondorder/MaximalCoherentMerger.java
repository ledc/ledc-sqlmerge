package be.ugent.ledc.sqlmerge.secondorder;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.Multiset;
import be.ugent.ledc.sqlmerge.Merger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Multiset Merger based on the Maximal Coherent Subset principle. This implementation
 * uses an optimized algorithm instead of the exhaustive (exponential) search.
 * @author antoon
 * @param <T> 
 */
public class MaximalCoherentMerger<T> implements Merger<Multiset<T>>
{
    @Override
    public Multiset<T> merge(Multiset<Multiset<T>> sources)
    {      
        Multiset<T> sum = new Multiset<>();
        
        for(Multiset<T> source: sources.keySet())
        {
            sum = sum.sum(source);
        }
        
        //Sort elements
        List<T> sortedList = toList(sum);
        Collections.reverse(sortedList);
        
        //Make a generator list
        Set<MaximalSubsetWrapper> generator = new HashSet<>();

        //Source Index
        int index = 0;

        for(Multiset<T> source: sources.keySet())
        {
            //Initialize source indices
            Multiset<Integer> sourceIndices = new Multiset<>();

            //Add initial index
            sourceIndices.add(index++);

            //Add a new rapper
            generator.add(new MaximalSubsetWrapper(source, sourceIndices));
        }
        
        //Initialize the solution
        Multiset<T> solution = new Multiset<>();
        
        //Index cache
        List<Multiset<Integer>> indexCache = new ArrayList<>();
        
        for(T element: sortedList)
        {
            //Indices
            Multiset<Integer> indices = new Multiset<>();
            
            //Find all sources that contain element
            for(MaximalSubsetWrapper wrapper: generator)
            {
                if(wrapper.getSubset().containsKey(element))
                {
                    indices = indices.union(wrapper.getSourceIndices());
                }
            }
            
            //Check cache
            boolean isMaximal = true;
            
            for(Multiset<Integer> cachedIndices: indexCache)
            {
                if(indices.isSubsetOf(cachedIndices) && !indices.equals(cachedIndices))
                {
                    isMaximal = false;
                    break;
                }
            }
            
            if(isMaximal)
            {
                solution.add(element);
                indexCache.add(indices);
            }
            
        }
        
        //Return
        return solution;
    }
    
    private class MaximalSubsetWrapper
    {
        private Multiset<T> subset;
        private Multiset<Integer> sourceIndices;

        public MaximalSubsetWrapper(){}

        public MaximalSubsetWrapper(Multiset<T> subset, Multiset<Integer> sourceIndices)
        {
            this.subset = subset;
            this.sourceIndices = sourceIndices;
        }

        public Multiset<T> getSubset()
        {
            return subset;
        }

        public void setSubset(Multiset<T> subset)
        {
            this.subset = subset;
        }

        public Multiset<Integer> getSourceIndices()
        {
            return sourceIndices;
        }

        public void setSourceIndices(Multiset<Integer> sourceIndices)
        {
            this.sourceIndices = sourceIndices;
        }
        @Override
        public int hashCode()
        {
            int hash = 3;
            hash = 79 * hash + (this.sourceIndices != null ? this.sourceIndices.hashCode() : 0);
            return hash;
        }

        @Override
        public boolean equals(Object obj)
        {
            if (obj == null)
            {
                return false;
            }
            if (getClass() != obj.getClass())
            {
                return false;
            }
            final MaximalSubsetWrapper other = (MaximalSubsetWrapper) obj;
            
            if (this.sourceIndices != other.sourceIndices && (this.sourceIndices == null || !this.sourceIndices.equals(other.sourceIndices)))
            {
                return false;
            }
            
            return true;
        }
    }
    
    /**
     * Returns a sorted list of elements according to descending multiplicity. Each objects occurs only one time in this list, regardless of its multiplicity.
     * @param multiset
     * @return Sorted {@link ArrayList} of elements in the core of the {@link Multiset}.
     */
    public ArrayList<T> toList(Multiset<T> multiset)
    {
        //Make list of keys
        ArrayList<T> list = new ArrayList<>();
        list.addAll(multiset.keySet());
        
        //Sort list
        Collections.sort(list, (T o1, T o2) -> Integer.compare(multiset.get(o1), multiset.get(o2)));

        //Return
        return list;
    }
}
