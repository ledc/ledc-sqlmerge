package be.ugent.ledc.sqlmerge.secondorder;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.datastructures.Multiset;
import be.ugent.ledc.sqlmerge.Merger;
import be.ugent.ledc.sqlmerge.MergingException;
import be.ugent.ledc.sqlmerge.SecondOrderMerger;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class MultisetMerger implements SecondOrderMerger
{
    private final Merger<Multiset<DataObject>> embeddedMerger;

    public MultisetMerger(Merger<Multiset<DataObject>> embeddedMerger)
    {
        this.embeddedMerger = embeddedMerger;
    }
    
    @Override
    public Dataset merge(List<Dataset> input, DataObject parentMerge, boolean mergeDuplicateElements) throws MergingException
    {
        Multiset<DataObject> embeddedMerge = embeddedMerger.merge(
            new Multiset(input
                .stream()
                .map(d -> applyParentMerge(d, parentMerge))
                .map(d -> new Multiset(d.getDataObjects()))
                .collect(Collectors.toSet())
            ));

        //If duplicate elements must be merged, we trim the bag
        if(mergeDuplicateElements)
            return new SimpleDataset(new ArrayList<>(embeddedMerge.keySet()));
        
        //Else we return each element present
        SimpleDataset mergedDataset = new SimpleDataset();
        
        for(DataObject o: embeddedMerge.keySet())
        {
            IntStream
                .rangeClosed(1, embeddedMerge.multiplicity(o))
                .mapToObj(i->o)
                .forEach(mergedDataset::addDataObject);
        }
        
        return mergedDataset;
    }

    public Merger<Multiset<DataObject>> getEmbeddedMerger()
    {
        return embeddedMerger;
    }    
}
