package be.ugent.ledc.sqlmerge.secondorder;

import be.ugent.ledc.core.datastructures.Multiset;
import be.ugent.ledc.sqlmerge.Merger;

public class UnionMerger<T> implements Merger<Multiset<T>>
{
    @Override
    public Multiset<T> merge(Multiset<Multiset<T>> input)
    {
        if(input == null || input.isEmpty())
            return new Multiset<>();
        
        //Initialize
        Multiset<T> result = null;

        //Iterate
        for(Multiset<T> source: input.keySet())
        {
            //Merge
            result = (result == null)
                ? source
                : result.union(source);
        }

        //Return
        return result;
    }
}
