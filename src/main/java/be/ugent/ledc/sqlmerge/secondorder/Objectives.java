package be.ugent.ledc.sqlmerge.secondorder;

import java.util.function.BiFunction;

public enum Objectives implements Objective
{
    MINUMUM((a,b) -> Math.min(a,b)),
    MEAN((a,b) -> (a+b) / 2.0),
    HARMONIC_MEAN((a,b) -> (2.0 * a * b) / (a + b)),
    GEOMETRIC_MEAN((a,b) -> Math.sqrt(a * b));
    
    private final BiFunction<Double,Double,Double> embedded;

    private Objectives(BiFunction<Double, Double, Double> embedded)
    {
        this.embedded = embedded;
    }

    @Override
    public double combine(double precision, double recall)
    {
        return embedded.apply(precision, recall);
    }
}
