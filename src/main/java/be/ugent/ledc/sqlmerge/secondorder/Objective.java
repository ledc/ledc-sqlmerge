package be.ugent.ledc.sqlmerge.secondorder;

import be.ugent.ledc.core.datastructures.Multiset;
import java.util.function.BinaryOperator;

public interface Objective<U>
{
    public default double calculate(Multiset<Multiset<U>> sources, Multiset<U> solution, BinaryOperator<Float> tnorm)
    {   
        double recall       = Calculator.recall(sources,solution,tnorm);
        double precision    = Calculator.precision(sources,solution,tnorm);
        
        return combine(precision, recall);
    }
    
    public double combine(double precision, double recall);
}
