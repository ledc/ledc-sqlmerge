package be.ugent.ledc.sqlmerge.secondorder;

import be.ugent.ledc.core.datastructures.Multiset;
import be.ugent.ledc.sqlmerge.firstorder.CoallesceMerger;
import java.util.Iterator;
import be.ugent.ledc.sqlmerge.Merger;
import be.ugent.ledc.sqlmerge.MergingException;

public class OptimalMerger <T> implements Merger<Multiset<T>>
{
    protected Merger<Multiset<T>> tieBreaker = new CoallesceMerger<>();
    
    private Objective<T> objective = Objectives.HARMONIC_MEAN;
    
    public OptimalMerger(Merger<Multiset<T>> tieBreaker, Objective<T> objective)
    {
        this.tieBreaker = tieBreaker;
        this.objective = objective;
    }
    
    public OptimalMerger(){}
    
    @Override
    public Multiset<T> merge(Multiset<Multiset<T>> sources) throws MergingException
    {
        //Basic Fusers
        Merger<Multiset<T>> union = new UnionMerger<>();
        Merger<Multiset<T>> intersection = new IntersectionMerger<>();
        
        //Construct upper and lower solution
        Multiset<T> upperSolution = union.merge(sources);
        Multiset<T> solution = intersection.merge(sources);

        //Keep track of the best solutions
        Multiset<Multiset<T>> bestSolutions = new Multiset<>();
        bestSolutions.add(new Multiset<>(solution));

        //Keep track of maximal F-value
        double optimum = objective.calculate(sources, new Multiset<>(), (Float a, Float b) -> (Float)Math.min(a,b));

        while(!solution.equals(upperSolution))
        {
            //Calculate recall of this solution
            double recall = Calculator.recall(sources, solution, (Float a, Float b) -> (Float)Math.min(a,b));

            //Find element(s) that have local recall equal to this recall (Note: tnorm=min implies that there are always such elements)
            Iterator<T> elementIterator = upperSolution.keySet().iterator();

            while(elementIterator.hasNext())
            {
                //Get next element
                T element = elementIterator.next();

                //Is local recall of this element equal to recall?
                if(Math.abs(Calculator.localRecall(sources, solution.get(element), element) - recall) < Double.MIN_VALUE)
                {
                    //Minimally increase multiplicity such that local recall increases
                    int adder = 1;
                    while(!(Calculator.localRecall(sources, solution.get(element) + adder, element) > recall) && solution.get(element) + adder <= upperSolution.get(element))
                    {
                        adder++;
                    }
                    solution.put(element, solution.get(element)+adder);
                }
            }

            //Evaluate the new solution
            double objectiveValue = objective.calculate(sources, solution, (Float a, Float b) -> Math.min(a,b));

            //Is this solution equally good?
            if(Math.abs(objectiveValue - optimum) < Double.MIN_VALUE)
            {
                //Add
                bestSolutions.add(new Multiset<>(solution));
            }
            //Is this solution better?
            else if(objectiveValue > optimum)
            {
                //Reset best solutions
                bestSolutions.clear();

                //Add better solutions
                bestSolutions.add(new Multiset<>(solution));

                //Update maximal harmonicMeanOptimization
                optimum = objectiveValue;
            }
        }

        //Return best solution
        return tieBreaker.merge(bestSolutions);
    }
    
    public Merger<Multiset<T>> getTieBreaker()
    {
        return tieBreaker;
    }

    /**
     * Setter for the tiebreaker.
     * @param tieBreaker 
     */
    public void setTieBreaker(Merger<Multiset<T>> tieBreaker)
    {
        this.tieBreaker = tieBreaker;   
    }

    public Objective getObjective()
    {
        return objective;
    }

    public void setObjective(Objective objective)
    {
        this.objective = objective;
    }
}
