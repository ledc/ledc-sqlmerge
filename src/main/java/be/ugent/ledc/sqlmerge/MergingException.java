package be.ugent.ledc.sqlmerge;

import be.ugent.ledc.core.LedcException;

public class MergingException extends LedcException
{
    public MergingException(String message)
    {
        super(message);
    }

    public MergingException(String message, Throwable cause)
    {
        super(message, cause);
    }

    public MergingException(Throwable cause)
    {
        super(cause);
    }

    public MergingException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
