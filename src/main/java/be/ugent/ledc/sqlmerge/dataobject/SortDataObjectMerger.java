package be.ugent.ledc.sqlmerge.dataobject;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.sqlmerge.firstorder.Selector;
import be.ugent.ledc.sqlmerge.firstorder.SortMerger;
import java.util.List;

public class SortDataObjectMerger extends SortMerger<DataObject>
{
    private final List<String> sortKey;
    
    public SortDataObjectMerger(List<String> sortKey, Selector selector)
    {
        super(DataObject.getProjectionComparator(sortKey), selector);
        this.sortKey = sortKey;
    }

    public List<String> getSortKey()
    {
        return sortKey;
    }
}
