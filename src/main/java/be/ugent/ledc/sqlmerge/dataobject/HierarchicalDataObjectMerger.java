package be.ugent.ledc.sqlmerge.dataobject;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.Multiset;
import be.ugent.ledc.sqlmerge.Merger;
import be.ugent.ledc.sqlmerge.MergingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * A merger for DataObjects that merges each attribute individually. Optionally,
 * some combinations of attributes can be indicated to be donated by the same object
 * to avoid awkward combinations to occur.
 * @author abronsel
 */
public class HierarchicalDataObjectMerger implements Merger<DataObject>
{
    private final Map<String, Merger<?>> attributeMergers;

    private final List<Set<String>> preservableSets;

    public HierarchicalDataObjectMerger()
    {
        this.attributeMergers = new HashMap<>();
        this.preservableSets = new ArrayList<>();
    }

    public HierarchicalDataObjectMerger(Map<String, Merger<?>> attributeFusers, List<Set<String>> preservableSets)
    {
        this.attributeMergers = attributeFusers;
        this.preservableSets = preservableSets;
    }
    
    public HierarchicalDataObjectMerger(Map<String, Merger<?>> attributeFusers)
    {
        this(attributeFusers, new ArrayList<>());
    }

    @Override
    public DataObject merge(Multiset<DataObject> input) throws MergingException
    {
        //Initialize the merged DataObject
        DataObject mergedObject = new DataObject();

        if (attributeMergers != null)
        {
            //For each attribute
            for (String attribute : attributeMergers.keySet())
            {
                //Get merger
                Merger attributeMerger = attributeMergers.get(attribute);

                //Make projection
                Multiset projection = new Multiset();

                for (DataObject inputObject : input.keySet())
                {
                    //Project record over attribute
                    projection.add(inputObject.get(attribute), input.multiplicity(inputObject));
                }

                //Merge attribute
                Object attributeMerge = attributeMerger.merge(projection);

                //Add to result
                mergedObject.set(attribute, attributeMerge);
            }
        }

        //Return
        return transform(mergedObject, input);
    }

    private DataObject transform(DataObject merged, Multiset<DataObject> input)
    {
        if (this.preservableSets != null && !this.preservableSets.isEmpty())
        {
            //Make a new solution
            DataObject pSolution = new DataObject();

            //For every cluster
            for (int i = 0; i < this.preservableSets.size(); i++)
            {
                //Fetch
                Set<String> preservableGroup = this.preservableSets.get(i);

                //Keep source votes
                int[] votes = new int[input.size()];

                //Keep a source counter
                int sourceCounter = 0;

                //Iterate over sources
                for (DataObject source : input.keySet())
                {
                    //Get the source multiplicity
                    int multiplicity = input.get(source);

                    //For any attribute in the cluster
                    for (String attribute : preservableGroup)
                    {
                        //Get the source attribute value
                        Object sValue = source.get(attribute);

                        //Get the solution attribute value
                        Object oValue = merged.get(attribute);

                        if (sValue != null && oValue != null && sValue.equals(oValue))
                            votes[sourceCounter] += multiplicity;
                        else if (sValue == null && oValue == null)
                            votes[sourceCounter] += multiplicity;
                    }

                    //Update source counter
                    sourceCounter++;
                }

                //The maximum number of votes
                int maxNumOfVotes = 0;

                //The index of the best source
                int bestSourceIndex = 0;

                //Find the source with the most number of votes
                for (int j = 0; j < votes.length; j++)
                {
                    if (votes[j] > maxNumOfVotes)
                    {
                        maxNumOfVotes = votes[j];
                        bestSourceIndex = j;
                    }
                }

                //Reset the source counter
                sourceCounter = 0;

                //Re-iterate
                for (DataObject source : input.keySet())
                {
                    //Find the best source
                    if (sourceCounter++ == bestSourceIndex)
                    {
                        //Iterate over attributes in the cluster
                        for (String attribute : preservableGroup)
                        {
                            //The preserved solution takes attributes equal to the best source
                            pSolution.set(attribute, source.get(attribute));
                        }
                        
                        //Break out
                        break;
                    }
                }
            }

            //INFO: Attributes not in a cluster are the same as the original solution
            //Get attribute names
            Iterator<String> attributeIterator = merged.getAttributes().iterator();

            //Iterate over all attributes
            while (attributeIterator.hasNext())
            {
                //Get the next attribute
                String attribute = attributeIterator.next();

                //If this attribute is not in the solution yet, copy it from the original solution
                if (!pSolution.getAttributes().contains(attribute))
                {
                    pSolution.set(attribute, merged.get(attribute));
                }
            }

            //Return the partial preserved solution
            return pSolution;
        }
        else
        {
            return merged;
        }
    }

    public void addAttributeMerger(String attributeName, Merger attributeMerger)
    {
        this.attributeMergers.put(attributeName, attributeMerger);
    }

    public void addPreservableGroup(HashSet<String> preservableGroup)
    {
        this.preservableSets.add(preservableGroup);
    }

    public Map<String, Merger<?>> getAttributeMergers()
    {
        return attributeMergers;
    }

    public List<Set<String>> getPreservableSets()
    {
        return preservableSets;
    }
}
