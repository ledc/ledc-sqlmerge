package be.ugent.ledc.sqlmerge.dataobject;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.Multiset;
import be.ugent.ledc.sqlmerge.Merger;
import java.util.Set;
import java.util.stream.Collectors;

public class DummyMerger implements Merger<DataObject>
{
    @Override
    public DataObject merge(Multiset<DataObject> input)
    {
        DataObject dummyMerge = new DataObject();
        
        Set<String> attributes = input
            .keySet()
            .stream()
            .flatMap(o->o.getAttributes().stream().filter(a->o.get(a) != null))
            .distinct()
            .filter(a -> input
                .keySet()
                .stream()
                .filter(o->o.get(a) != null)
                .map(o->o.project(a))
                .distinct()
                .count() == 1)
            .collect(Collectors.toSet());
        
        for(String a: attributes)
        {
            dummyMerge.set(
                a,
                input
                    .keySet()
                    .stream()
                    .filter(o->o.get(a)!=null)
                    .map(o->o.get(a))
                    .findFirst()
                    .get()
            );
        }
        return dummyMerge;
    }
    
}
