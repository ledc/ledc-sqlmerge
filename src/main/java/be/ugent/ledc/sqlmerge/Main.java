package be.ugent.ledc.sqlmerge;

import be.ugent.ledc.core.LedcException;
import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.config.parameter.Parameter;
import be.ugent.ledc.core.config.parameter.Flag;
import be.ugent.ledc.core.binding.BindingException;
import be.ugent.ledc.core.binding.DataReadException;
import be.ugent.ledc.core.binding.csv.CSVBinder;
import be.ugent.ledc.core.binding.csv.CSVDataReader;
import be.ugent.ledc.core.binding.csv.CSVProperties;
import be.ugent.ledc.core.binding.jdbc.JDBCBinder;
import be.ugent.ledc.core.binding.jdbc.PreparedJDBCDataReader;
import be.ugent.ledc.core.binding.jdbc.agents.PersistentDatatype;
import be.ugent.ledc.core.binding.jdbc.agents.DMLGenerator;
import be.ugent.ledc.core.binding.jdbc.schema.TableSchema;
import be.ugent.ledc.core.config.parameter.ParameterTools;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.FixedTypeDataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.sqlmerge.dataobject.HierarchicalDataObjectMerger;
import be.ugent.ledc.sqlmerge.firstorder.BalancedMerger;
import be.ugent.ledc.sqlmerge.firstorder.dynamic.CountEstimator;
import be.ugent.ledc.sqlmerge.io.JSON;
import be.ugent.ledc.sqlmerge.propagation.ConsoleFactory;
import be.ugent.ledc.sqlmerge.propagation.MergeStrategy;
import be.ugent.ledc.sqlmerge.propagation.Console;
import be.ugent.ledc.sqlmerge.propagation.PropagationEngine;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import static java.util.stream.Collectors.mapping;
import static java.util.stream.Collectors.toSet;
import java.util.stream.Stream;

public class Main
{
    public static final Parameter<File> CONFIG_FILE = new Parameter<File>(
            "configFile",
            "--c",
            true,
            (s) -> new File(s),
            "JSON file that holds JDBC binder and merge strategy."
    );

    public static final Parameter<File> OUTPUT_FILE = new Parameter<File>(
            "outputFile",
            "--o",
            true,
            (s) -> new File(s),
            "SQL file to write the output SQL script to."
    );
    public static final Parameter<File> MERGE_FILE = new Parameter<File>(
            "mergeFile",
            "--m",
            true,
            (s) -> new File(s), "Csv file that contains the groups of keys to merge.");

    public static final Parameter<String> TABLE_NAME = new Parameter<String>(
            "tableName",
            "--t",
            true,
            Function.identity(),
            "Table in which the merging starts.");

    public static final Flag VERBOSE_FLAG = new Flag("verbose", "--v", "Verbose output.");
    public static final Flag INTERVENE_FLAG = new Flag("intervene", "--i", "Allow interventions in second order merges.");

    public static final String NAME = "sqlmerge";
    public static final String MERGE_ATTRIBUTE = "#merge";

    public static final Parameter<?>[] PARAMETERS = new Parameter<?>[]
    {
        CONFIG_FILE,
        OUTPUT_FILE,
        MERGE_FILE,
        TABLE_NAME
    };

    public static final Flag[] FLAGS = new Flag[]
    {
        VERBOSE_FLAG,
        INTERVENE_FLAG
    };

    public static void main(String[] args)
    {
        loadVerifiers();

        try
        {
            //Parse arguments
            File configFile = ParameterTools.extractParameter(CONFIG_FILE, args);
            File outputFile = ParameterTools.extractParameter(OUTPUT_FILE, args);
            File mergeFile = ParameterTools.extractParameter(MERGE_FILE, args);
            String tableName = ParameterTools.extractParameter(TABLE_NAME, args);

            Console.verbose = ParameterTools.extractFlag(VERBOSE_FLAG, args);

            //Check for unknown arguments
            String unknownArg = ParameterTools.unknownArgumentCheck(args, PARAMETERS, FLAGS);
            
            if(unknownArg != null)
            {
                ConsoleFactory
                    .getConsole()
                    .println("Unknown argument " + unknownArg, Console.ANSI_RED);
                printUsage();
                System.exit(-1);
            }
            
            //Get binder
            ConsoleFactory.getConsole().printlnVerbose("Reading JDBC binder");
            JDBCBinder binder = readBinder(configFile);

            TableSchema tableSchema = binder.getSchema().get(tableName);
            
            if(tableSchema == null)
                throw new DataReadException("Could not read data from table " + tableName);
            
            //Get merge strategy
            ConsoleFactory.getConsole().printlnVerbose("Reading merge strategy");
            MergeStrategy mergeStrategy = JSON.readMergeStrategy(configFile);
            
            //Data to merge
            ConsoleFactory.getConsole().printlnVerbose("Reading objects to merge");
            Map<String, Set<DataObject>> dataToMerge = readMergeFile(
                mergeFile,
                binder,
                tableName);
            
            ConsoleFactory.getConsole().printlnVerbose("Balanced merge check");
            balancedMergeCheck(mergeStrategy, dataToMerge, binder, tableSchema);
            
            //Construct the propagation engine
            PropagationEngine propagationEngine = new PropagationEngine(
                mergeStrategy, 
                binder,
                ParameterTools.extractFlag(INTERVENE_FLAG, args)
            );
            
            PreparedJDBCDataReader objectReader = buildPreparedReader(
                binder,
                null,
                binder
                    .getSchema()
                    .get(tableName)
            );
            
            PrintWriter writer = new PrintWriter(
                new OutputStreamWriter(
                    new FileOutputStream(outputFile),
                    StandardCharsets.UTF_8)
            );
            
            for(String mergeId: dataToMerge.keySet())
            {
                String jobName = dataToMerge
                    .get(mergeId)
                    .stream()
                    .map(o->o.toString())
                    .collect(Collectors.joining(",", "(", ")"));
                
                Dataset mergeGroup = new SimpleDataset();
                        
                for(DataObject keyValue: dataToMerge.get(mergeId))
                {
                    mergeGroup = mergeGroup
                        .unionAll(
                            objectReader
                                .readContractedData(keyValue)
                        );
                }
                
                ConsoleFactory.getConsole().println("");
                ConsoleFactory.getConsole().println("Starting merge in " + tableName + " for objects " + jobName);
                ConsoleFactory.getConsole().println("");
                
                List<String> sqlScript = propagationEngine.startMerge(tableSchema, mergeGroup);
                
                //Write a comment that indicates the objects for which the merge applies
                writer.println("-- Merge in " + tableName + " for rows " + jobName);
                
                //Write the script
                sqlScript.stream().forEach(writer::println);
                
                writer.println("");
                writer.flush();
            }
            
            //Close IO connections
            writer.close();
            objectReader.closeConnection();
            propagationEngine.close();
        }
        catch (LedcException pex)
        {
            ConsoleFactory.getConsole().println(pex.getMessage(), Console.ANSI_RED);
            printUsage();
        }
        catch (IOException | SQLException  ex)
        {
            ConsoleFactory.getConsole().println(ex.getMessage(), Console.ANSI_RED);
        }
    }

    private static void printUsage()
    {
        ConsoleFactory.getConsole().println("Usage: java -jar " + NAME + ".jar <parameters> <flags>");
        ConsoleFactory.getConsole().println("\nParameters:");

        for (Parameter p : PARAMETERS)
        {
            ConsoleFactory
                    .getConsole()
                    .println(
                            p.getName() + "\t"
                            + p.getShortName() + "\t"
                            + p.getDescription()
                    );
        }

        ConsoleFactory.getConsole().println("\nFlags:");
        for (Flag f : FLAGS)
        {
            ConsoleFactory
                    .getConsole()
                    .println(
                            f.getName() + (f.getName().length()<=8 ? "\t\t" : "\t")
                            + f.getShortName() + "\t"
                            + f.getDescription()
                    );
        }
    }
    
    private static void loadVerifiers()
    {
        CONFIG_FILE.addVerifier((f) -> f.exists(), "Could not find config file.");
        CONFIG_FILE.addVerifier((f) -> f.getName().endsWith(".json"), "Config file must be a .json file.");

        OUTPUT_FILE.addVerifier((f) -> f.getName().endsWith(".sql"), "Output file must be a .sql file.");

        MERGE_FILE.addVerifier((f) -> f.exists(), "Could not find merge file.");
        MERGE_FILE.addVerifier((f) -> f.getName().endsWith(".csv"), "Merge file must be a .csv file.");
    }
    
    private static JDBCBinder readBinder(File configFile) throws ParseException, FileNotFoundException
    {
        JDBCBinder binder = JSON.readJDBCBinder(configFile);

        //Retrieve binder password
        ConsoleFactory
            .getConsole()
            .println("Enter binder password:");

        binder
            .getRdb()
            .setPassword(
                ConsoleFactory
                .getConsole()
                .readPassword()
            );

        return binder;
    }
    
    private static Map<String, Set<DataObject>> readMergeFile(File mergeFile, JDBCBinder binder, String mergeTable) throws DataReadException, BindingException
    {
        //Read the raw data
        FixedTypeDataset<String> dataToMerge = new CSVDataReader(
            new CSVBinder(
                new CSVProperties(true, ",", '"'),
                mergeFile))
            .readData();
        
        //Get details on the merge table
        TableSchema mergeTableSchema = binder.getSchema().get(mergeTable);
        
        if(dataToMerge.select(o -> o.getAttributes().containsAll(mergeTableSchema.getPrimaryKey())).getSize() == 0)
        {
            throw new DataReadException("Data to merge in file '" + mergeFile.getName() + "' does not contain primary key values of table '" + mergeTable + "'");
        }
        
        Dataset convertedData = PersistentDatatype
                .convertTextData(dataToMerge, mergeTableSchema);
        
        return convertedData
            .getDataObjects()
            .stream()
            .collect(Collectors
                .groupingBy(
                    o -> o.getString(MERGE_ATTRIBUTE),
                    mapping(o -> o.project(mergeTableSchema.getPrimaryKey()), toSet())) 
                )
            ;
        
    }

    private static PreparedJDBCDataReader buildPreparedReader(JDBCBinder binder, Set<String> projection, TableSchema tableSchema) throws SQLException, BindingException
    {
        return new PreparedJDBCDataReader(
            binder,
            new DMLGenerator(
                binder
                    .getRdb()
                    .getVendor()
                    .getAgent()
                    .escaping()
            )
            .prepareKeySelectCode(
                projection,
                tableSchema,
                binder
                    .getRdb()
                    .getSchemaName()),
            tableSchema.getPrimaryKey()
        );
    }

    private static void balancedMergeCheck(MergeStrategy mergeStrategy, Map<String, Set<DataObject>> dataToMerge, JDBCBinder binder, TableSchema tableSchema) throws SQLException, BindingException, DataReadException
    {
        if(mergeStrategy.getFirstOrderHandler(tableSchema.getName()) != null
          && mergeStrategy
            .getFirstOrderHandler(tableSchema.getName())
            .getMerger() instanceof HierarchicalDataObjectMerger)
        {
            //Cast
            HierarchicalDataObjectMerger hm = (HierarchicalDataObjectMerger)mergeStrategy
                .getFirstOrderHandler(tableSchema.getName())
                .getMerger();
            
            for(String a: hm.getAttributeMergers().keySet())
            {
                if(hm.getAttributeMergers().get(a) instanceof BalancedMerger)
                {
                    ConsoleFactory.getConsole().printlnVerbose("Start learning of partial order for attribute '" + a + "'");
                    
                    PreparedJDBCDataReader reader = buildPreparedReader(
                        binder,
                        Stream.of(a).collect(Collectors.toSet()),
                        tableSchema);
                    
                    ConsoleFactory.getConsole().increaseDepth();
                    ConsoleFactory.getConsole().printlnVerbose("Constructing confusion clusters");
                    
                    List<Set> listOfClusters = new ArrayList<>();
                
                    for(String mergeId: dataToMerge.keySet())
                    {
                        Set cluster = new HashSet();
                        
                        for(DataObject keyValue: dataToMerge.get(mergeId))
                        {
                            cluster.addAll(
                                reader
                                .readContractedData(keyValue)
                                .getDataObjects()
                                .stream()
                                .filter(o -> o.get(a) != null)
                                .map(o -> o.get(a))
                                .collect(Collectors.toSet())
                            );    
                        }
                        listOfClusters.add(cluster);
                    }
                    
                    ConsoleFactory.getConsole().printlnVerbose("Constructing partial order");
                    
                    ((BalancedMerger)hm.getAttributeMergers().get(a))
                        .learn(
                            listOfClusters,
                            new CountEstimator());
                        
                    ConsoleFactory.getConsole().printlnVerbose("done");
                    
                    ConsoleFactory.getConsole().decreaseDepth();
                    reader.closeConnection();
                }
            }
        }
    }
}
