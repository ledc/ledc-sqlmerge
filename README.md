# ledc-sqlmerge

## Why
When a database contains duplicate data, there comes a time you might want to clean that database by *merging* duplicate rows into a single row.
In a relational database, such a merge operation will involve at least one <tt>delete</tt> statement and that delete can **propagate** throughout the entire database.
For a complex database schema, handling this propagation rapidly becomes troublesome.

## What
This repository is part of ledc, which is short for *Lightweight Engine for Data quality Control*. 
It is a modular framework designed for data quality monitoring and improvement in several different ways.

The current repo contains ledc-sqlmerge: a simple tool to assist in the construction a SQL script that models merges of rows in a table of a relational database and handles propagations of relationships.
Currently, ledc-core supports only binding to PostgreSQL database, but in principle, sqlmerge is able to support any relational database system.
The main features of sqlmerge are:

* **SQL scripting**: the result of using the tool is a SQL script that models all requested merges, reflects all conflict resolutions that were made and handles all propagated merge operations.

* **Merge strategy**: several merge strategies can be used to describe how conflicts should be resolved automatically.
When auto-merging is not possible, user interaction will be used to resolve such conflicts.

* **Constraint validation**: the result of merge operations is validated against constraints defined in the database.

* **Propagation handling**: <tt>delete</tt> statements involved in merges are propagated to tables with a foreign key relationship.
Both one-to-one and one-to-many relationships are supported.
For the latter, a range of *second order merge* strategies are available. 

* **Partial Order learning**: in some cases, attributes must be merged by means of a partial order that reflects which values are more general or more specific than others.
A typical example is the type of a restaurant.
Such a partial order can be learned automatically and can be used to perform merges via the *balanced* strategy.  
 
## Getting started

As all current and future components of ledc, ledc-sqlmerge is written in Java.
To run it, as from version 1.2, you require Java 17 or higher and Maven.
Just pull the code from this repository and build it with Maven.
Alternatively, you can have a look at the registry to download an executable JAR file.
Documentation can be found [here](docs/index.md).

## References

Below are listed some of the publications where the core ideas of ledc-sqlmerge where conceived.

* Antoon Bronselaer, Marcin Szymczak, Slawomir Zadrozny and Guy De Tré, Dynamical order construction in data fusion, *Information Fusion* (**27**), p. 1-18, (**2016**)

* Antoon Bronselaer, Daan Van Britsom and Guy De Tré, Propagation of data fusion, *IEEE Transactions on Knowledge and Data Engineering*, (**27**). p.1330-1342 (**2015**)

* Antoon Bronselaer, Daan Van Britsom and Guy De Tré, A framework for multiset merging, *Fuzzy Sets and Systems* (**191**), p. 1-20, (**2012**)
