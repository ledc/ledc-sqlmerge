-- Merge in item for rows ({item_id=1},{item_id=2})
delete from "example_3"."book" where "book_id"=2;
update "example_3"."book" set "book_title"='The Name of the Rose', "isbn"='978-074-93-97050' where "book_id"=1;
update "example_3"."item_instance" set "item_id"=1 where "item_instance_id"=5;
update "example_3"."item_instance" set "item_id"=1 where "item_instance_id"=3;
update "example_3"."item_instance" set "item_id"=1 where "item_instance_id"=6;
update "example_3"."item_instance" set "item_id"=1 where "item_instance_id"=4;
delete from "example_3"."item" where "item_id"=2;

-- Merge in item for rows ({item_id=3},{item_id=4})
delete from "example_3"."dvd" where "dvd_id"=4;
update "example_3"."item_instance" set "item_id"=3 where "item_instance_id"=8;
update "example_3"."item_instance" set "item_id"=3 where "item_instance_id"=9;
delete from "example_3"."item" where "item_id"=4;

