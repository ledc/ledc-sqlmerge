set search_path to 'example_3';

create table customer
(
	customer_id integer primary key,
	name text,
	zip text
);

insert into customer (customer_id, name, zip) values
(1, 'Ryan Bleynolds', '9000'), 
(2, 'Brian Bleynolds', '9000');

create table item
(
	item_id integer primary key,
);

insert into item (item_id) values (1),(2),(3),(4);

create table book
(
	book_id integer primary key,
	book_title text,
	author text,
	isbn text,
	unique(isbn),
	foreign key (book_id) references item(item_id)
);

insert into book (book_id, book_title, author, isbn)
values
(1, 'The name of the rose', 'Umberto Eco', '9780749397050'),
(2, 'The Name of the Rose', 'Umberto Eco', '978-074-93-97050');

create table dvd
(
	dvd_id integer primary key,
	dvd_title text,
	director text,
	foreign key (dvd_id) references item(item_id)
);

insert into dvd (dvd_id, dvd_title, director)
values
(3, 'The name of the rose', 'Jean-Jacques Annaud'),
(4, 'Der name der Rose', 'Jean-Jacques Annaud');

create table item_instance
(
	item_instance_id integer primary key,
	item_id integer,
	status text check (status in ('available', 'loaned', 'reserved')),
	location text,
	foreign key (item_id) references item(item_id)
);

insert into item_instance (item_instance_id, item_id, status)
values
(1, 1, 'loaned'),
(2, 1, 'available'),
(3, 2, 'available'),
(4, 2, 'available'),
(5, 2, 'available'),
(6, 2, 'reserved'),
(7, 3, 'reserved'),
(8, 4, 'available'),
(9, 4, 'loaned');

create table loan
(
	customer_id integer,
	item_instance_id integer,
	loan_start_time timestamp,
	loan_end_time timestamp,
	primary key (item_instance_id,loan_start_time),
	
	foreign key (customer_id) references customer(customer_id),
	foreign key (item_instance_id) references item_instance(item_instance_id),
	check (loan_end_time>loan_start_time),
	unique(item_instance_id,loan_end_time)
);

insert into loan (customer_id, item_instance_id, loan_start_time, loan_end_time)
values
(1, 5, '10/18/2021 10:00', '10/26/2021 10:00'),
(1, 7, '10/18/2021 10:00', '10/26/2021 10:00'),
(1, 1, '10/26/2021 10:00', null),
(1, 9, '10/26/2021 10:00', null);

