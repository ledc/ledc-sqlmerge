## Example 1: a multi-valued attribute

This example demonstrates the basic idea of recursive merging.
The files necessary to reconstruct this example can be found in the folder [example 1](./example_1).

The basic idea in this simple example is to consider an entity <tt>person</tt> with a multi-valued attribute (MVA) <tt>hobbies</tt>.
This latter MVA is modelled in a separate table and is linked to the entity table by means of a 1-N relationship.
The diagram below illustrates the table structure considered in this simple example.

```mermaid
erDiagram
person{
    int person_id
	text name
}
hobbies{
    int person_id
	text hobby
}
person ||--|{ hobbies: "hasHobby"
```

When inspecting the [db-setup](./example_1/db-setup.sql), you will see the tables are populated with the following commands:

```sql
insert into person (person_id, name) values
(1, 'John'),
(2, 'Jon'),
(3, 'Jones'),
(4, 'Johnny'),
(5, 'El Johnerino');

insert into hobbies (person_id, hobby) values
(1,'Snooker'),
(1,'Bowling'),
(1,'Football'),
(2,'Bowling'),
(2,'Snooker'),
(2,'Cycling'),
(3,'Reading'),
(3,'Bowling'),
(4,'Snookering'),
(4,'Football'),
(4,'Reading'),
(5,'Cycling'),
(5,'Snooker');
```

Suppose now we want to merge the five rows in table <tt>person</tt>, then a first order merge will provide us with one row that replaces these five rows.
Next, the FK-relationship from <tt>hobbies</tt> to <tt>person</tt> will cause a recursive merge to <tt>hobbies</tt>.
In the current [configuration](./example_1/config.json), we use the `Optimal` strategy and this will keep a hobby if it is reported by sufficiently many sources.
In this particular case, hobbies "Snooker" and "Bowling" are preserved.
As a result, with the **Optimal** strategy, the following script is the output of sqlmerge.
```sql
-- Merge in person for rows ({person_id=1},{person_id=2},{person_id=3},{person_id=4},{person_id=5})
delete from "example_1"."hobbies" where "person_id"=4 and "hobby"='Snookering';
delete from "example_1"."hobbies" where "person_id"=4 and "hobby"='Football';
delete from "example_1"."hobbies" where "person_id"=4 and "hobby"='Reading';
delete from "example_1"."hobbies" where "person_id"=5 and "hobby"='Cycling';
delete from "example_1"."hobbies" where "person_id"=2 and "hobby"='Cycling';
delete from "example_1"."hobbies" where "person_id"=3 and "hobby"='Reading';
delete from "example_1"."hobbies" where "person_id"=1 and "hobby"='Football';
delete from "example_1"."hobbies" where "person_id"=5 and "hobby"='Snooker';
delete from "example_1"."hobbies" where "person_id"=2 and "hobby"='Snooker';
delete from "example_1"."hobbies" where "person_id"=2 and "hobby"='Bowling';
delete from "example_1"."hobbies" where "person_id"=3 and "hobby"='Bowling';
delete from "example_1"."person" where "person_id"=2;
delete from "example_1"."person" where "person_id"=3;
delete from "example_1"."person" where "person_id"=4;
delete from "example_1"."person" where "person_id"=5;
update "example_1"."person" set "name"='Jones' where "person_id"=1;
```
It is now easy to experiment with some other strategies.
For example, the **MaxCoherent** strategy will ensure that none of the five sets of hobbies has an empty intersection with the merged set of hobbies.
More particular, it will result in hobbies "Snooker", "Bowling", "Reading" and "Football".
This translates to the following script being produced, where the `insert` statement now adds a hobby to the set of hobbies linked to person_id `1`.
```sql
-- Merge in person for rows ({person_id=1},{person_id=2},{person_id=3},{person_id=4},{person_id=5})
delete from "example_1"."hobbies" where "person_id"=4 and "hobby"='Snookering';
delete from "example_1"."hobbies" where "person_id"=5 and "hobby"='Cycling';
delete from "example_1"."hobbies" where "person_id"=2 and "hobby"='Cycling';
delete from "example_1"."hobbies" where "person_id"=5 and "hobby"='Snooker';
delete from "example_1"."hobbies" where "person_id"=2 and "hobby"='Snooker';
delete from "example_1"."hobbies" where "person_id"=2 and "hobby"='Bowling';
delete from "example_1"."hobbies" where "person_id"=3 and "hobby"='Bowling';
delete from "example_1"."hobbies" where "person_id"=4 and "hobby"='Football';
delete from "example_1"."hobbies" where "person_id"=4 and "hobby"='Reading';
delete from "example_1"."hobbies" where "person_id"=3 and "hobby"='Reading';
insert into "example_1"."hobbies"("hobby","person_id") values ('Reading',1);
delete from "example_1"."person" where "person_id"=2;
delete from "example_1"."person" where "person_id"=3;
delete from "example_1"."person" where "person_id"=4;
delete from "example_1"."person" where "person_id"=5;
update "example_1"."person" set "name"='Jones' where "person_id"=1;
```

A third option might be to consider the (default) **Union** strategy, in which all hobbies are preserved.
One particular problem that arises in that case, is the fact that two hobbies are variations of one another: "Snooker" and "Snookering".
This could be solved afterwards by updating the table <tt>hobbies</tt> and use the `on conflict` clause to determine the behaviour in case the primary constraint is violated.

An alternative is to immediately *intervene* in the second order merge that takes place in table <tt>hobbies</tt>.
This is done by enabling the [intervene flag](../docs#using-the-command-line-interface) when sqlmerge is run.
Doing so, with the Union strategy to perform the second order merge, will halt the procedure and ask for input:
```console
   | Reduced merge has 6 objects
   | 1. {hobby=Snooker,person_id=1}
   | 2. {hobby=Bowling,person_id=1}
   | 3. {hobby=Cycling,person_id=1}
   | 4. {hobby=Snookering,person_id=4}
   | 5. {hobby=Football,person_id=1}
   | 6. {hobby=Reading,person_id=1}
   | Are there any tuples that must be merged here? Yes or No
```

Choosing "Yes" will prompt for a list of hobbies to be merged, which allow to merge hobbies "Snooker" and "Snookering".
The following SQL script is then the output:
```sql
-- Merge in person for rows ({person_id=1},{person_id=2},{person_id=3},{person_id=4},{person_id=5})
delete from "example_1"."hobbies" where "person_id"=5 and "hobby"='Snooker';
delete from "example_1"."hobbies" where "person_id"=2 and "hobby"='Snooker';
delete from "example_1"."hobbies" where "person_id"=2 and "hobby"='Bowling';
delete from "example_1"."hobbies" where "person_id"=3 and "hobby"='Bowling';
delete from "example_1"."hobbies" where "person_id"=5 and "hobby"='Cycling';
delete from "example_1"."hobbies" where "person_id"=2 and "hobby"='Cycling';
insert into "example_1"."hobbies"("hobby","person_id") values ('Cycling',1);
delete from "example_1"."hobbies" where "person_id"=4 and "hobby"='Football';
delete from "example_1"."hobbies" where "person_id"=4 and "hobby"='Reading';
delete from "example_1"."hobbies" where "person_id"=3 and "hobby"='Reading';
insert into "example_1"."hobbies"("hobby","person_id") values ('Reading',1);
delete from "example_1"."hobbies" where "person_id"=4 and "hobby"='Snookering';
delete from "example_1"."person" where "person_id"=2;
delete from "example_1"."person" where "person_id"=3;
delete from "example_1"."person" where "person_id"=4;
delete from "example_1"."person" where "person_id"=5;
update "example_1"."person" set "name"='Jones' where "person_id"=1;
```
