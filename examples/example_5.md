## Example 5: partial order learning

In this example, we show how the first order merge in the starting table can be equipped with a balanced merger.
This is mainly useful if there is an attribute for which it can be assumed there is some unknown partial order that expresses a specificity relationship.
In addition, using a balanced merger only makes sense if there is a sufficient amount of merges that need to be done in the starting table in order to ensure there is sufficient data to learn from.
An example where a balanced merger is useful, is the <tt>type</tt> attribute in the restaurant dataset, originally found [here](https://www.cs.utexas.edu/users/ml/riddle/data.html) and modified slightly for our purposes [here](./example_5/restaurant.csv).

The main idea of learning the partial order is to check how many times some value `v` appears in combination with some other values.
If it turns out that `v` appears with many different other values, it is assumed `v` is somehow a generic value that can be easily replaced with many others.
Opposed to that, if `v` does not appear with many other different values, it is assumed `v` is rather specific.
Based on this idea, a binary relationship `R` is constructed that meets the requirements of a partial order (i.e., reflexive, anti-symmetric and transitive).
The learning process can be tweaked by setting the parameter [cutoff](./example_5/config.json), which serves as a lower bound on the number of times two values most co-occur before they are considered in the partial order.
Higher values of `cutoff` will result in a more accurate partial order, but allows to compare less values and will thus lead to more interactions with the user.
Lower values of `cutoff` will allow more direct comparisons between values, but might contain some inaccurate decisions.
The partial order that is learned can be consulted when the `verbose` flag is activated.

When running sqlmerge with the given [config file](./example_5/config.json), a balanced merger is requested for attribute <tt>type</tt> and the `cutoff` parameter is 2.
Enabling the `verbose` flag will lead to the following output:
```console
   | Partial order learning result (k=2):
   | french bistro specifies french
   | delicatessen specifies delis
   | cafeterias specifies american
   | french (new) specifies american
   | french (new) specifies french
   | french (classic) specifies french
   | steakhouses specifies american
```
which can be visualized as follows:

```mermaid
graph TB
    dd("delis")-->d("delicatessen");
    
    fn("french (new)");
    fc("french (classic)");
    a("american");
    f("french");
    a-->caf("cafeterias")
    a-->fn;
    f-->fn;
    f-->fc;
    a-->st("steakhouses");

    style a fill:#ffffff;
    style d fill:#ffffff;
    style dd fill:#ffffff;
    style f fill:#ffffff;
    style fn fill:#ffffff;
    style fc fill:#ffffff;
    style caf fill:#ffffff;
    style st fill:#ffffff;
```
During merging, the balanced merger can use this information to decide in favor of type `steakhouses` over `american`.
