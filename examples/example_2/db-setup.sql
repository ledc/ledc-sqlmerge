set search_path to 'example_2';

-- DDL part
create table publication
(
	publication_id int primary key,
	title text
);

create table author
(
	author_id int primary key,
	first_name text,
	surname text,
	institute text
);

create table authorship
(
	authorship_id serial primary key,
	author_id int,
	publication_id int,
	rank int check (rank > 0 and rank <= 50),
	
	unique(author_id, publication_id)
);

alter table authorship add constraint fk_authorship_publication foreign key (publication_id) references publication(publication_id);
alter table authorship add constraint fk_authorship_author foreign key (author_id) references author(author_id);
alter table authorship add constraint unique_rank unique (publication_id, rank);

-- Create some data
insert into publication (publication_id, title) values
(1, 'This is my paper'),
(2, 'This is my paper too'),
(3, 'I really love this paper');

insert into author (author_id, first_name, surname) values
(1, 'Brian', 'Bleynolds'),
(2, 'Brian', 'Bleeynolds'),
(3, 'Hue', 'Jackman'),
(4, 'Hugh', 'Jackmon');

insert into authorship (publication_id, author_id, rank) values
(1,1,1),
(1,3,2),
(2,1,2),
(2,4,1),
(3,2,2),
(3,3,1);
