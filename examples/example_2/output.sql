-- Merge in publication for rows ({publication_id=1},{publication_id=2},{publication_id=3})
delete from "test"."authorship" where "authorship_id"=8;
update "test"."authorship" set "publication_id"=1, "rank"=2 where "authorship_id"=12;
delete from "test"."authorship" where "authorship_id"=7;
update "test"."authorship" set "publication_id"=1, "rank"=1 where "authorship_id"=9;
update "test"."authorship" set "rank"=3 where "authorship_id"=12;
update "test"."authorship" set "publication_id"=1 where "authorship_id"=11;
update "test"."authorship" set "publication_id"=1, "rank"=4 where "authorship_id"=10;
delete from "test"."publication" where "publication_id"=2;
delete from "test"."publication" where "publication_id"=3;
update "test"."publication" set "title"='This is my paper too' where "publication_id"=1;

