## Example 2: a many-to-many relationship with constraints

This example demonstrates the recursive merging on a many-to-many relationship in the presence of some constraints.
The files necessary to reconstruct this example can be found in the folder [example 2](./example_2).

The example is basically a slightly modified version of the introductory example with publications and authors. 
The diagram below illustrates the table structure considered here.

```mermaid
erDiagram

publication{
    int publication_id
	text title
}
author{
    int author_id
	text first_name
    text surname
    text institute
}
authorship{
    int authorship_id
    int publication_id
	int author_id
	int rank
}
author ||--|{ authorship: "hasWritten"
publication ||--|{ authorship: "writtenBy"
```
The [db-setup](./example_2/db-setup.sql) learns that the primary key constraint in <tt>authorship</tt> is on <tt>authorship_id</tt>.
In addition, there is a unique constraint on <tt>{author_id, publication_id}</tt> as well as on <tt>{rank, publication_id}</tt>.
This is a typical example of a surrogate key, where some additional constraints are introduced to safeguard consistency. 
The tables are populated as follows:

```sql
insert into publication (publication_id, title) values
(1, 'This is my paper'),
(2, 'This is my paper too'),
(3, 'I really love this paper');

insert into author (author_id, first_name, surname) values
(1, 'Brian', 'Bleynolds'),
(2, 'Brian', 'Bleeynolds'),
(3, 'Hue', 'Jackman'),
(4, 'Hugh', 'Jackmon');

insert into authorship (publication_id, author_id, rank) values
(1,1,1),
(1,3,2),
(2,1,2),
(2,4,1),
(3,2,2),
(3,3,1);
```

Suppose now we want to merge the three rows in table <tt>publication</tt>, then a first order merge will provide us with one row that replaces these three rows.
In the [merge strategy](./example_2/config.json) used here, there is no first order merge and user input will be requested to resolve this.
Let's assume now we choose as `publication_id=1`.

The FK-relationship from <tt>authorship</tt> to <tt>publication</tt> will cause a recursive merge to <tt>authorship</tt>.
Because of the surrogate key, the current [configuration](./example_2/config.json) enables `foreignKeyProjection`.
To obtain some flexibility in resolving violations of unique constraints, the option `pushParentMerges` is set to `false`.
Furthermore, we choose the default `Union` strategy here to retain the maximal amount of rows in <tt>authorship</tt>.

Because `foreignKeyProjection` is used, rows in table <tt>authorship</tt> are identified by their combination of foreign keys, which boils down to <tt>{author_id, publication_id}</tt>.
In the [merge step](../docs/merge-strategy.md#the-merge-step), rows in <tt>authorship</tt> are projected over these attributes, after which they are combined with the second order merger (in this case, union).
During the [reduction step](../docs/merge-strategy.md#the-reduction-step), the values for <tt>publication_id</tt> are set to `1` and it will then be observed that there are two rows for `author_id=1` as well as for `author_id=3`.
These rows will be recursively merged with a first order merge in <tt>authorship</tt> and any conflicts/violations will be avoided.

After making some choices regarding the rank, the following script is an example output:
```sql
-- Merge in publication for rows ({publication_id=1},{publication_id=2},{publication_id=3})
delete from "test"."authorship" where "authorship_id"=8;
update "test"."authorship" set "publication_id"=1, "rank"=2 where "authorship_id"=12;
delete from "test"."authorship" where "authorship_id"=7;
update "test"."authorship" set "publication_id"=1, "rank"=1 where "authorship_id"=9;
update "test"."authorship" set "rank"=3 where "authorship_id"=12;
update "test"."authorship" set "publication_id"=1 where "authorship_id"=11;
update "test"."authorship" set "publication_id"=1, "rank"=4 where "authorship_id"=10;
delete from "test"."publication" where "publication_id"=2;
delete from "test"."publication" where "publication_id"=3;
update "test"."publication" set "title"='This is my paper too' where "publication_id"=1;
```
