create schema if not exists example_4;

set search_path to example_4;

-- create a table with a self-reference
create table treetable
(
	id integer primary key,
	name text not null,
	parent_id integer,
	foreign key (parent_id) references treetable(id)
);

-- insert some data
insert into treetable (id, parent_id, name) values
(1, null, 'root'),
(2, 1, 'first root child'),
(3, 1, 'second root child'),
(4, 3, 'node 4'),
(5, 3, 'node 5'),
(6, 4, 'node 6'),
(7, 4, 'node 7');