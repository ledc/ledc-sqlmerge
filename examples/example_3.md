## Example 3: an inheritance structure

This example demonstrates the recursive merging in a database with an inheritance structure.
The files necessary to reconstruct this example can be found in the folder [example 3](./example_3).

The example shows a use case of a library that loans items, where several item types (e.g., books, dvds...) are possible.
Each item can have multiple instances (e.g., several copies of the same book).

```mermaid
erDiagram

customer{
    int customer_id
	text name
    text zip
}
item{
    int item_id
}
book{
    int book_id
    text book_title
	text author
	isbn text
}
dvd{
    int dvd_id
    text dvd_title
	text directory
}
item_instance{
    int item_instance_id
    int item_id
    text status
    text location
}
loan{
    int customer_id
    int item_instance_id
    datetime loan_start_time
    datetime loan_end_time
}
customer ||--|{ loan: "loans"
item_instance ||--|{ loan: "loanedBy"
item ||--|{ item_instance: "instanceOf"
item ||--|| book: "isA"
item ||--|| dvd: "isA"
```
The [db-setup](./example_3/db-setup.sql) provides some additional information about this schema.
For example, the primary key constraint in <tt>loan</tt> is on <tt>{item_instance_id, loan_start_time}</tt> which implies that the combination of foreign keys (i.e. <tt>{item_instance_id, customer_id}</tt>) is not a good choice as projection key.
In addition, there is a check constraint on the attribute <tt>status</tt> in table <tt>item_instance</tt> and the <tt>isbn</tt> has a unique constraint.
The tables are populated as follows:

```sql
insert into customer (customer_id, name, zip) values
(1, 'Ryan Bleynolds', '9000'), 
(2, 'Brian Bleynolds', '9000');

insert into item (item_id) values (1),(2),(3),(4);

insert into book (book_id, book_title, author, isbn)
values
(1, 'The name of the rose', 'Umberto Eco', '9780749397050'),
(2, 'The Name of the Rose', 'Umberto Eco', '978-074-93-97050');

insert into dvd (dvd_id, dvd_title, director)
values
(3, 'The name of the rose', 'Jean-Jacques Annaud'),
(4, 'Der name der Rose', 'Jean-Jacques Annaud');

insert into item_instance (item_instance_id, item_id, status)
values
(1, 1, 'loaned'),
(2, 1, 'available'),
(3, 2, 'available'),
(4, 2, 'available'),
(5, 2, 'available'),
(6, 2, 'reserved'),
(7, 3, 'reserved'),
(8, 4, 'available'),
(9, 4, 'loaned');

insert into loan (customer_id, item_instance_id, loan_start_time, loan_end_time)
values
(1, 5, '10/18/2021 10:00', '10/26/2021 10:00'),
(1, 7, '10/18/2021 10:00', '10/26/2021 10:00'),
(1, 1, '10/26/2021 10:00', null),
(1, 9, '10/26/2021 10:00', null);
```

There are now different places where we can merge rows together.
First, in table <tt>item</tt>, items 1 and 2 can be merged, as well as items 3 and 4.
This will lead to recursive merging in tables <tt>book</tt> and <tt>dvd</tt>.
However, the primary keys of these tables are also the foreign keys to table <tt>item</tt>.
That means the relationship is one-to-one and the recursive merge to these tables is therefore a first order merge.
At the same time, there will also be a recursive merge to table <tt>item_instance</tt>, which will be a second order merge as the relationship is one-to-many.
By using the `Union` strategy, the following script is an example output:

```sql
-- Merge in item for rows ({item_id=1},{item_id=2})
delete from "example_3"."book" where "book_id"=2;
update "example_3"."book" set "book_title"='The Name of the Rose', "isbn"='978-074-93-97050' where "book_id"=1;
update "example_3"."item_instance" set "item_id"=1 where "item_instance_id"=5;
update "example_3"."item_instance" set "item_id"=1 where "item_instance_id"=3;
update "example_3"."item_instance" set "item_id"=1 where "item_instance_id"=6;
update "example_3"."item_instance" set "item_id"=1 where "item_instance_id"=4;
delete from "example_3"."item" where "item_id"=2;

-- Merge in item for rows ({item_id=3},{item_id=4})
delete from "example_3"."dvd" where "dvd_id"=4;
update "example_3"."item_instance" set "item_id"=3 where "item_instance_id"=8;
update "example_3"."item_instance" set "item_id"=3 where "item_instance_id"=9;
delete from "example_3"."item" where "item_id"=4;
```

Second, it is also possible to identify some duplicate entries in <tt>item_instance</tt>.
So we might run sqlmerge a second time, but now with starting table <tt>item_instance</tt>, to merge item instances 1 and 3.
Doing so, again taking the safe `Union` strategy, the following script is example output:
```sql
-- Merge in item_instance for rows ({item_instance_id=1},{item_instance_id=3})
delete from "example_3"."item_instance" where "item_instance_id"=3;
```
