We provide some simple examples here of cases where some merge operation is required and where the propagation to other tables is explained.
The examples are best considered in the order we present them here.

- [Example 1](example_1.md) demonstrates a very simple case with two tables: one entity table and one multi-valued attribute table.
This example demonstrates some differences in the [second order strategies](../docs/merge-strategy.md#second-order-mergers) and shows where the intervene flag can be useful.

- [Example 2](example_2.md) is an elaborate version of the introductory example with publications and authors.
The example demonstrates the interaction with constraints and show the usefulness of [foreign key projection](../docs/merge-strategy.md#projection-key)

- [Example 3](example_3.md) shows a more comprehensive example where an inheritance structure and a many-to-many relationship are used.
The example shows a case where recursive merging is first order (one-to-one relationship) and illustrates how multiple runs of sqlmerge can be combined to clean databases more extensively.

- [Example 4](example_4.md) illustrates that sqlmerge also works in case of self-references (typically used for modelling tree structures).

- [Example 5](example_5.md) demonstrates the usage of a balanced merger and the learning of a partial order on some training data.
