-- Merge in person for rows ({person_id=1},{person_id=2},{person_id=3},{person_id=4},{person_id=5})
delete from "example_1"."hobbies" where "person_id"=4 and "hobby"='Snookering';
delete from "example_1"."hobbies" where "person_id"=4 and "hobby"='Football';
delete from "example_1"."hobbies" where "person_id"=4 and "hobby"='Reading';
delete from "example_1"."hobbies" where "person_id"=5 and "hobby"='Cycling';
delete from "example_1"."hobbies" where "person_id"=2 and "hobby"='Cycling';
delete from "example_1"."hobbies" where "person_id"=3 and "hobby"='Reading';
delete from "example_1"."hobbies" where "person_id"=1 and "hobby"='Football';
delete from "example_1"."hobbies" where "person_id"=5 and "hobby"='Snooker';
delete from "example_1"."hobbies" where "person_id"=2 and "hobby"='Snooker';
delete from "example_1"."hobbies" where "person_id"=2 and "hobby"='Bowling';
delete from "example_1"."hobbies" where "person_id"=3 and "hobby"='Bowling';
delete from "example_1"."person" where "person_id"=2;
delete from "example_1"."person" where "person_id"=3;
delete from "example_1"."person" where "person_id"=4;
delete from "example_1"."person" where "person_id"=5;
update "example_1"."person" set "name"='Jones' where "person_id"=1;

