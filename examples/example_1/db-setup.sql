create schema if not exists example_1;
set search_path to example_1;

create table person
(
	person_id integer primary key,
	name text
);

create table hobbies
(
	person_id integer,
	hobby text,
	primary key(person_id, hobby),
	foreign key (person_id) references person(person_id)
);

insert into person (person_id, name) values
(1, 'John'),
(2, 'Jon'),
(3, 'Jones'),
(4, 'Johnny'),
(5, 'El Johnerino');

insert into hobbies (person_id, hobby) values
(1,'Snooker'),
(1,'Bowling'),
(1,'Football'),
(2,'Bowling'),
(2,'Snooker'),
(2,'Cycling'),
(3,'Reading'),
(3,'Bowling'),
(4,'Snookering'),
(4,'Football'),
(4,'Reading'),
(5,'Cycling'),
(5,'Snooker');