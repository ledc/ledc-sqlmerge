## Example 4: self references

In this example, a self-referencing table is considered.
Such self-referencing tables allow to model tree structures where leaf nodes can be at arbitrary depth.
The files necessary to reconstruct this example can be found in the folder [example 4](./example_4).

The idea in this example is to consider a table <tt>treetable</tt> that models a tree structure.
The diagram belows show this table definition, where there is a FK relationship from <tt>parent_id</tt> to <tt>id</tt>.

```mermaid
erDiagram
treetable{
    int id
	text name
    int parent_id
}

treetable ||--|{ treetable: "hasChildren"
```

When inspecting the [db-setup](./example_4/db-setup.sql), you will see the table is populated as follows:

```sql
insert into treetable (id, parent_id, name) values
(1, null, 'root'),
(2, 1, 'first root child'),
(3, 1, 'second root child'),
(4, 3, 'node 4'),
(5, 3, 'node 5'),
(6, 4, 'node 6'),
(7, 4, 'node 7');
```
The data in this table represents a very simple tree structure shown in the diagram below on the left.

```mermaid
graph TB
    subgraph "Merged Tree"
        r("root")-->f("first root child");
        r-->s("second root child");
        f-->n6("node 6");
        f-->n7("node 7");
        s-->n5("node 5");
    end

    subgraph "Original Tree"
        root("root")-->first("first root child");
        root-->second("second root child");
        second-->node4("node 4");
        second-->node5("node 5");
        node4-->node6("node 6");
        node4-->node7("node 7");
    end

    style first fill:#ffffff, stroke:#2bbde9;
    style node4 fill:#ffffff, stroke:#2bbde9;
    style root fill:#ffffff, stroke:#000000;
    style second fill:#ffffff, stroke:#000000;
    style node5 fill:#ffffff, stroke:#000000;
    style node6 fill:#ffffff, stroke:#000000;
    style node7 fill:#ffffff, stroke:#000000;

    style f fill:#ffffff, stroke:#DA3B1C;
    style r fill:#ffffff, stroke:#000000;
    style s fill:#ffffff, stroke:#000000;
    style n5 fill:#ffffff, stroke:#000000;
    style n6 fill:#ffffff, stroke:#000000;
    style n7 fill:#ffffff, stroke:#000000;
```
We are now interested in merging two subtrees of this structure together.
More precisely, we want to merge the subtrees corresponding to the nodes marked with a blue border in the diagram above.
We will then need to make a choice on the new parent (either the root node or the second root child).
Moreover, we also need to decide on how the predecessors of both nodes must be combined.
These predecessors are in the table identified by the children of the nodes we are merging, which means a second order merge will be fired here.
When choosing the `Union` strategy, we keep both subtrees, resulting in the tree structure show on the right in the diagram above.
The corresponding sql script is given by:
```sql
-- Merge in treetable for rows ({id=2},{id=4})
update "example_2"."treetable" set "parent_id"=2 where "id"=6;
update "example_2"."treetable" set "parent_id"=2 where "id"=7;
delete from "example_2"."treetable" where "id"=4;
```
