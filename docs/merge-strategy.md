# The merge strategy

The merge strategy is basically a collection of strategies to perform merging in tables.
As explained in the [introductory example](index.md), we hereby make a distinction between to different types of merge operations:

* A **first order** merge takes a set of tuples and merges them into a single tuple.
These operators are used in the initial merge and in recursive merges if the relationship is one-to-one.
The latter case is detected if the foreign key is equal to the primary of the referring table.

* A **second order** merge takes a *set* of sets of tuples and merges them into a single set of tuples.
These operators are used in recursive merges if the relationship is one-to-many.

The schema below shows an overview of the current available mergers.
Merge strategies might be added in future version of ledc-sqlmerge.

```mermaid
graph TB
    merger("Merger");
    first("First order:<br>merges rows");
    second("Second order:<br>merges sets of rows");
    flags("Options:<br>foreignKeyProjection<br>secondOrderSubsumption<br>pushParentMerges");
    second-.-flags;
    
    osorted("SortSelect:<br>sorts objects and selects one");
    ohierarchy("Hierarchical:<br>merges attributes");
    
    merger-->first;
    merger-->second;

    first-->osorted;
    first-->ohierarchy;

    ohierarchy-.->attr["Attribute mergers:<br>Select<br>Majority<br>Coallesce<br>Balanced (only for initial merge)"];

    second-->intersect("Intersection");
    second-->mcs("MaxCoherent");
    second-->optimal("Optimal");
    second-->cascade("Cascade");
    second-->union("Union");
    
    
    style first fill:#ffffff, stroke:#2bbde9;
    style second fill:#ffffff, stroke:#2bbde9;
    
    style attr fill:#ffffff;
    style flags fill:#ffffff;
```
## First order mergers

First order mergers are strategies to combine a set of tuples/rows from a table into a *single* row.
Each merge task always starts with a first order merge in some starting table.
As indicated in the diagram above, there are two ways of defining such a first order merge:

- A **sortSelect** merge will sort the rows according to some sort key (specified as a group of attributes).
This sorting is done *lexicographically*: rows are first sorted by their values for the first attribute, then by their values for the second and so on.
From the sorted rows, a fixed rows is selected by a *selection* strategy.
Currently, the available selection strategies are `FIRST`, `MIDDLE` and `LAST`.

- A **hierarchical** merge will merge all of the attributes *individually*.
As such, a hierarchical merge is specified by a mapping from attribute names to attribute mergers.
An attribute that does *not* occur in the mapping will not be merged automatically and will be resolved via conflict resolution if multiple alternatives are available.
Currently, the following attribute mergers are available:

    * Select: similar to a sorted merge, except the sort key is now the attribute itself. 
    * Majority: selects the most frequent value, where ties are broken randomly
    * Coallesce: selects a random value that is not-null.
    * Balanced: uses a learned partial order and considers a selection strategy based on specificity. More information can be found [here](#balanced-merging).

#### Conflict resolution

Performing a first order merge can result in a *merge conflict* because either (i) a merge strategy was not specified for some attribute in the hierarchical strategy or (ii) a merge strategy was specified, but could not make a decision.
When a conflict is observed in a first order merge, conflict resolution is initiated.
It will first be verified if a decision can be taken automatically.
If the conflict is due to a missing merge strategy for an attribute and there is only one possible values that is not null, then this value is selected automatically.
When auto-merging is not possible, user input will be requested to make a decision.

#### Merge validation

When a merge is computed, be it completely automatically or via interaction with the user, the result will be validated.
Merge validation will search for the following constraints and verify them:

- check constraints
- not null constraints
- unique constraints
- foreign key constraints

In addition, it will verify that the primary key value in the merge is one of the original primary key values.
If a merge is violating a constraint, it will be prompted for change and after changes are made, revalidation is done.
This repeats until no violations are found.

#### Balanced merging

One specific merge strategy for attributes, is balanced merging.
This strategy is only allowed in the table where the initial merging is done.
The reason for this is that balanced merging *learns* a partial order on all groups of rows that are prompted for merging.
The partial order is considered as a model for generality and when merges must be made, the balanced merging will use the most *specific* value that is comparable to all alternatives.

## Second order mergers

When a first order merge takes place in a table that is referred to by another table, recursive merging will be executed.
The merge strategy allows to specify how merging in referencing tables needs to be done.
There are two options:

- If the reference is a *one-to-one* relationship, the recursive merge is again a first order merge.
- If the reference is a *one-to-many* relationship, the recursive merge is a second order merge.

The choice is automatically made by inspecting the foreign and primary key of the refering table.
In the case of a second order merge strategy, a few options are available (see the [example](../examples) section for some illustrations):

- With the **Intersection** strategy, the sets of referring tuples will be intersected and the intersection is the result of the second order merge.
This is a very strict strategy and discards all information, unless it appears in each of the referring sets of tuples.

- With the **Union** strategy (default), the union of sets of referring tuples is the result of the second order merge. This is the safest option as it will not keep a maximal amount of information.

- If there is trust in a specific source of information, the **Cascade** strategy can be used. This does exactly the same as the cascade strategy of the relational database: deletes of rows in one table are propagated to all referring tables.

- If there are many rows that are merged, then there will also be many sets of linked tuples. To maintain only information that is sufficiently trustworthy, an **Optimal** merge can be considered.
Hereby, the occurence of a row in a set is considered a vote for that row and an optimality criterion based on precision/recall balancing is used to optimally combine those votes.

- In a similar spirit of the previous, the **MaxCoherent** strategy also aims to combine sets by means of the [maximal coherent subset](https://www.irit.fr/~Didier.Dubois/Papers0804/DDC_IEEE07.pdf) principle.

The main difficulty with second order merging, is that the above strategies can not just be applied to the rows in the referencing table directly.
Under normal circumstances, this referencing table has a primary key and each row is unique, rendering most strategies above useless.
The trick is to *project* the rows over an [identifying part](#projection-key) before they are passed to the second order merger.
When the second order merger has computed its result, the original rows are then mapped to their identifying parts in the merge result.
At this point, some orginal rows will be deleted and some will need to be [merged recursively](#recursive-merges) with other rows because they have the same identifying parts.
After these potentially recursive merges, we obtain a *reduced* second order merge.
This reduced merge is then validated against the constraints defined in the database.
When violations are found, there are two options: delete some rows or update some rows.
Just as with violations in the case of a first order merge, this process repeats until no violations are found.
The main flow of second order merging is shown in the diagram below.

```mermaid
graph LR
    rows1{{Set of rows 1}};
    rows2{{Set of rows 2}};
    etc{{...}};
    rows4{{Set of rows N}};

    subgraph "Merge step"
        project("Second Order Merge<br>(Projection)");
    end

    rows1-->project;
    rows2-->project;

    rows4-->project;

    merge1{{Merged rows}};
    merge2{{Reduced rows}};
    merge3{{Final rows}};
    project-->merge1;
    subgraph "Reduction step"
        match("Match with orginals<br>(Recursive FO merging)");
    end
    merge1-->match;
    match-->merge2;
    subgraph "Validation step"
        validate("Validate<br>(Delete or Update rows)");
    end
    merge2-->validate;
    validate-->merge3;

    style project fill:#ffffff, stroke:#2bbde9;
    style match fill:#ffffff, stroke:#2bbde9;
    style validate fill:#ffffff, stroke:#2bbde9;


    style etc fill:#ffffff, stroke:#ffffff;
    
```
In the sections below, we provide more detail on this proces.

#### The merge step

In the diagram shown above, the merge step requires to project sets of rows on some identifying part.
This part is called the *projection key* here.
The projection key is a group of attributes over which tuples are projected *before* they are processed with a second order merger.
Let's consider two variations of the publication example.

```mermaid
erDiagram
publication1{
    int publication_id
	text title
}
author1{
    int author_id
	text name
}
authorship1{
    int publication_id
	int author_id
	int rank
}
author1 ||--|{ authorship1: "hasWritten"
publication1 ||--|{ authorship1: "writtenBy"

publication2{
    int publication_id
	text title
}
author2{
    int author_id
	text name
}
authorship2{
    int authorship_id
    int publication_id
	int author_id
	int rank
}
author2 ||--|{ authorship2: "hasWritten"
publication2 ||--|{ authorship2: "writtenBy"
```
You might note that the first case (shown on the left) is the case we already saw in the introduction.
In this case, the primary key is composed of the attributes <tt>publication_id</tt> and <tt>author_id</tt>.
The common sense is here to project over the primary key, apply the merged value for <tt>publication_id</tt> and then apply the second order merger to these projections.
If we would not do this, conflicts in the attribute <tt>rank</tt> would potentially disturb the merge as this is not an identifying attribute for the authorship.
The purpose of second order merging is *not* to resolve those conflicts, but rather decide, for each author, if that author should be included or not.

**Foreign key projection.** In the second case (shown on the right), the <tt>authorship</tt> table is now equipped with a surrogate key <tt>authorship_id</tt>.
This is problematic in the sense that, even if we replace all values of <tt>publication_id</tt> with the merged value from the referenced table <tt>publication</tt>, all rows will still be unique.
In this case, the more logical thing to do is to project over the combination of foreign keys.
This behavior can be enforced by setting `"foreignKeyProjection":true` in the config file.
This option can be chosen for each table individually.

#### The reduction step

After the merge step, we obtain an intermediate merge result.
The next step is to link the rows appearing in the original sets to rows in this intermediate result.
Two situations are now of interest:

1. Some original rows might not link anymore to the results and those rows are staged for deletion.
That is, the second order merge strategy decided that this row should not appear in the final result anymore.

2. There may exist *multiple* tuples that map to the *same* projection.
When these tuple have *differences* for some attributes, these differences need to be resolved.
This resolution can be done by a new recursive merge, which is now first order and acts on the same table where the second order merge took place.

As an example, suppose we would merge two publications that are duplicate and the table <tt>authorship</tt> looks as follows.
| publication_id | author_id | rank |
| ------ | ------ | ------- |
| 1 | 1 | 1 |
| 1 | 2 | 2 |
| 2 | 1 | 2 |
| 2 | 2 | 1 |

In the snippet above, the table <tt>authorship</tt> mentions the same two authors for both publications: the authors with <tt>author_id</tt> `1` and `2`.
Now suppose the recursive second order merge to <tt>authorship</tt> is done with the `union` strategy.
Now, there are two tuples with <tt>author_id</tt> equal to `1`, but with *different* values for attribute <tt>rank</tt>.
Hence, these tuples require merging, more specifically, a first order merge in <tt>authorship</tt>.

**Second order subsumption.** In determining *which* tuples are linked to the same tuple in the second order merge as exemplified above, there are two options.
The default option is that each attribute in the projection key should have the same value.
This is what we've seen above.
In some cases, especially when there are many foreign keys, it might be useful to also include tuples for which the projection key is *subsumed* by another projection key.
In that case, tuples are checked, for each attribute, to have the same values for tuple *or potentially a null value*.
If this second behaviour is desired, it can be enforced by setting `"secondOrderSubsumption":true` in the config file.
It should be noted however that this strategy should be handled with care and only be applied if there is a suspicion that some foreign keys are only sporadically missing.
Again, the option of second order subsumption can be chosen for each table individually.

**Skipping the reduction step.** In some cases, it may desireable to skip the reduction step.
This is especially the case when the foreign key projection is set to true and there is no unique constraint on the combination of foreign keys.
In that particular case, some of the original sets of tuples might already contain the same combination of foreign key values more than once.
If there is a need to keep all those rows, you can choose to set `"mergeDuplicateElements":false` in the config file.

**Pushing parent merges.** A final option to consider, is whether self-recursive merges immediately enforce the parent merge.
In this example above, there are two values for <tt>publication_id</tt>: `1` and `2`.
However, we already made a decision on how these values must be merged during the first order merge in table <tt>publication</tt>.
The question is whether or not we should immediately apply this "parent" merge.
By default, the answer is yes, but doing might sometimes be constraining when there are unique constraints in place.
If there is a need for postponing the parent merge application to a latter stage, you can set `"pushParentMerges":false` in the config file.

Note that the choice about which rows should be recursively merged is made automatically.
If you want to manually intervene in this choice after the merge step, you can enforce this behaviour by running sqlmerge with the `intervene` flag.

#### The validation step

The rows we obtain after the reduction step might violate some constraints.
Therefore, the final step involves validation of constraints for the second order merge.
Any constraints that are violated can either be resolved by updating or deleting rows from the second order merge.




