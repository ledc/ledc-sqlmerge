# ledc-sqlmerge

## Introduction

The basic problem we consider, is that of a relational database in which (i) some rows of a table <tt>t</tt> need to be merged and (ii) such that <tt>t</tt> is referenced by other tables via foreign key constraints.
As an example, consider the following simple database diagram that describes the many-to-many relationship between authors and publications.

```mermaid
erDiagram
publication{
    int publication_id
	text title
}
author{
    int author_id
	text name
}
authorship{
    int publication_id
	int author_id
	int rank
}
author ||--|{ authorship: "hasWritten"
publication ||--|{ authorship: "writtenBy"
```
Now suppose there are two rows in the table <tt>publication</tt> that are duplicate (i.e., they describe the same publication) and must be merged.
An example snippet of data could be the table shown below.

| publication_id | title |
| ------ | ------ |
| 1644 | My awesome publication |
| 2289 | My publication, so awesome I added it twice |

Merging these rows means one of them will be removed and the other one potentially updated.
An example merge could be obtained by executing the following simple SQL script:
```sql
update publication
	set title = 'My awesome merged publication'
	where publication_id = 1644;
delete from publication
	where publication_id = 2289;
```
and when this simple script is executed, the result will a table like shown below:

| publication_id | title |
| ------ | ------ |
| 1644 | My awesome merged publication |

An interesting question is now: ''What happens with rows in the table <tt>authorship</tt>?''.
The answer is that if we do nothing, it depends on the way the foreign key constraint from <tt>authorship</tt> to <tt>publication</tt> was defined.
The SQL standard comes with a few strategies that can be used when referenced primary keys are deleted:
* The DBMS may prevent the original delete from happening: `RESTRICT` or `NO ACTION`, where the latter will postpone verification to the moment constraints are checked.
* The DBMS can set the referencing foreign key to a null value (`SET NULL`) or the default value (`SET DEFAULT`).
* Deletes can be *cascaded*: the deletion of a primary key values is then propagated by deleting all referencing rows as well.

With sqlmerge, we try to do better than these strategies by *propagating* the merge to referencing tables.
For one-to-one relationships, this comes down to a recursive merge step in a referencing table.
For one-to-many relationships, the same is true, but now we need to merge *sets* of rows rather that indiviual rows.
The former operation, where we merge multiple rows into one row, is called a *first order* merge, while the latter one, where we merge multiple *sets* of rows into one set, is called a *second-order* merge.

The basic idea of sqlmerge is that you as a user provide strategies to perform both first and second order merges in an automated way.
These strategies are specified by means of a [merge strategy](merge-strategy.md).
If at some point a merge cannot be done, a *conflict* is risen and user input will be necessary to resolve that conflict.
All auto-merges and resolved conflicts are stored and used to compile a simple SQL script that is written to a separate file.
The script will *never* be executed against the database: it can be reviewed, revised, executed on a test server first... before you decide to finally execute it.

## Using sqlmerge

### Setting up

If you want to use sqlmerge to build merge scripts for a database, there are a few things you need to setup first.

- In order to build the script, sqlmerge will consult the metadata (i.e., table names, column names, column types and constraints) in order to build an internal representation of the database schema on which it can reason.
In addition, it will read relevant snippets of data.
Hence, when you compose credentials for the database, you need to make sure the database use has the *sufficient rights* to access metadata.
Note that this in particular means that propagation will **only work** if foreign keys are properly defined in the database.

- Currently, the tool has a simple command-line interface that generates many text output.
For better interpretation colored output it used.
When running sqlmerge on Windows, it might be necessary to set a registry flag that enables the interpretation of ANSI escape code.
This can be done by executing `reg add HKCU\Console /v VirtualTerminalLevel /t REG_DWORD /d 1`.
You can read more about this [here](https://docs.microsoft.com/en-us/windows/console/console-virtual-terminal-sequences?redirectedfrom=MSDN).
Note: if you execute the setting of the registry key in a command prompt, you might need to restart that prompt before the flag becomes active.

### Using the command-line interface

Currently, ledc-sqlmerge works as a simple CLI tool.
The snippet below shows the usage of this interface, which basically takes for parameters.

```shell
Usage: java -jar sqlmerge.jar <parameters> <flags>

Parameters:
configFile      --c     JSON file that holds JDBC binder and merge strategy.
outputFile      --o     SQL file to write the output SQL script to.
mergeFile       --m     Csv file that contains the groups of keys to merge.
tableName       --t     Table in which the merging starts.

Flags:
verbose         --v     Verbose output.
intervene       --i     Allow interventions in second order merges.
```
The **output file** is basically the file in which the SQL script that is built, will be written.
It is a clear design choice *not* to execute this script directly to the target database for a number of reasons.
It is advisable to execute the script first on a test environment to check if no unexpected consequences happen.
Currently, not all constraints are verified to build the script: check constraints and not null constraints are verified, but unique constraints are not.

The **table name** is the name of the table in the database where the merge starts.
This is necesssary because the propagation engine needs a location to start looking for rows that need merging.

The optional **verbose** flag will generate more extensive output.

The optional **intervene** flag allows to interupt second order merges and force additional merge operations dynamically.
You should use this flag only if you already have a good understanding of the mechanics of merging and propagating.

The last two parameters require a bit more explanation and are detailed below.

#### The config file

The **config file** is a simple JSON file in which the connection details for the database as well as the [merge strategy](merge-strategy.md) are encoded.
An example in terms of the publication example is shown below.

```json
{
    "Binder": {
        "schema": "test",
        "database": "test",
        "port": "5432",
        "vendor": "POSTGRESQL",
        "host": "localhost",
        "encoding": "UTF-8",
        "user": "postgres"
    },
    "MergeStrategy": {
        "firstOrderMergers":
        {"publication":
            {
                "mergerType": "Hierarchical",
                "NestedMergers": {
                    "publication_id":
                    {
                        "mergerType": "Select",
                        "Selector": "FIRST"
                    }
                }
            },
         "author":
            {
                "mergerType": "SortSelect",
                "SortKey": ["author_id"],
                "Selector": "FIRST"
            }
        },
        "secondOrderMergers":
        {
            "authorship":
            {
                "mergerType": "Union",
                "secondOrderSubsumption": false,
                "foreignKeyProjection": false,
                "pushParentMerges" : true
            }
        }
    }
}
```

#### The merge file

The **merge file** is a csv file that describes the basic merge operations that need to be executed in the starting table.
The starting table is specified by means of `tableName` parameter.
The header of the file *must* be equal to the primary key attributes of the starting table plus one special column named `#merge`.
Rows with identical values for the column `#merge` are rows that will be merged in the starting table.
The delimiter of the file *must* be a comma and, optionally, the quoting symbol `"` can be used when deemed necessary.

For example, to merge the two publications we considered earlier, the merge file should look as below.
```csv
#merge,publication_id
1,1644
1,2289
```

